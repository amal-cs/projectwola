/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import messaging from '@react-native-firebase/messaging';
import { name as appName } from './app.json';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';

PushNotification.configure({
  onRegister: function (token) {
  },
  onNotification: function (notification) {
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },
});

messaging().setBackgroundMessageHandler(async remoteMessage => {
  PushNotification.localNotification({
    title: remoteMessage.data.title, 
    message: remoteMessage.data.body, 
    playSound: true,
    soundName: "default",
  });
});

AppRegistry.registerComponent(appName, () => App);
