import React, { useState, useContext } from 'react';
import { View, StyleSheet, Text, TextInput, FlatList, Image } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/functions';
import { Button, Card } from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LottieView from 'lottie-react-native';
import { Context as HomeContext } from '../context/HomeContext';

const findFriends = () => {

    const [searchTerm, setSearchTerm] = useState('');
    const [result, setResult] = useState([]);
    const { state } = useContext(HomeContext);
    const [loading, setLoading] = useState(false);

    const searchFriend = () => {
        setLoading(true);
        firestore().collection("Users").where("name", "==", searchTerm.toLowerCase()).get().then(snapshot => {
            var friend = []
            if(snapshot.size > 0){
                snapshot.forEach(dataSnapshot => {
                    friend.push(dataSnapshot.data());
                    setLoading(false);
                    setResult(friend);
                });
            }else{
                setLoading(false);
                setResult([]);
            }
            
        }).catch(error => {
            setResult([]);
        });
    };

    const friendExists = (uId) => {
        return state.userData.friendList.some((el) => {
            return el.id === uId;
        });
    };

    const pendFriendExists = (uId) => {
        return state.userData.pendFriendReq.some((el) => {
            return el === uId;
        });
    }

    const sendFriendRqst = (uId, tokens) => {
        
        firestore().collection("Users").doc(uId).update({
            friendReq: firestore.FieldValue.arrayUnion(state.userData.id)
        });
        firestore().collection("Users").doc(state.userData.id).update({
            pendFriendReq: firestore.FieldValue.arrayUnion(uId)
        });
        var data = firebase.functions().httpsCallable('sendFriendRequest');
        data({
            name: state.userData.name,
            tokens: tokens
        }).then(res => {
            console.log(res);
        });
    };

    return (
        <View>
            <View style={styles.searchWrap}>
                <View style={styles.searchSection}>
                    <Ionicons style={styles.searchIcon} name="ios-search" size={20} color="#A9AEAF" />
                    <TextInput autoCapitalize="none" autoCorrect={false} style={styles.searchInput} placeholder="Search" value={searchTerm} onChangeText={setSearchTerm} />
                </View>
                <Button color="#3D7282" style={{ height: 40, borderRadius: 20, marginLeft: 10 }} mode="contained" onPress={() => searchFriend()}>Go</Button>
            </View>
            {loading ? (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/7774-loading.json')} autoPlay loop />) :
                result.length > 0 ?
                <FlatList
                    data={result}
                    style={{ marginTop: 12 }}
                    keyExtractor={(item) => `${item.id}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.friendView}>
                                <View style={styles.innerFriendView}>
                                    <Card style={styles.friendCard}>
                                        <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                    </Card>
                                    <Text style={styles.friendText}>{item.name}</Text>
                                </View>
                                {friendExists(item.id) &&
                                    <Button mode="outlined" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {

                                    }}>Friend</Button>
                                }
                                {!friendExists(item.id) && !pendFriendExists(item.id) &&
                                    <Button mode="contained" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                        sendFriendRqst(item.id, item.tokens)
                                    }}>Add</Button>
                                }
                                {pendFriendExists(item.id) &&
                                    <Button mode="outlined" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {

                                    }}>Pending</Button>
                                }
                            </View>
                        );
                    }}
                /> : <LottieView style={{ width: 190, height: 150, alignSelf: "center" }} source={require('../assets/10223-search-empty.json')} autoPlay loop />
            }

        </View>
    );
}

const styles = StyleSheet.create({
    searchSection: {
        flexDirection: 'row',
        alignSelf: "stretch",
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 50,
        flexGrow: 2
    },
    searchIcon: {
        padding: 10,
    },
    searchInput: {
        height: 40,
        alignSelf: "stretch",
        borderRadius: 50,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: "#fff"
    },
    searchWrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "stretch",
        marginTop: 12
    },
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(findFriends);