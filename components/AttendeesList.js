import React,{useContext} from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import { Card, Button } from 'react-native-paper';
import { Context as NewEventContext} from '../context/NewEventContext';

const attendeesList = () => {
    const {state: eventState, removeFriend} = useContext(NewEventContext);

    return(
        <View>
            <FlatList
                data={eventState.attendeeList}
                keyExtractor={(item) => `${item.id}`}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.friendView}>
                            <View style={styles.innerFriendView}>
                                <Card style={styles.friendCard}>
                                    <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                </Card>
                                <Text style={styles.friendText}>{item.name}</Text>
                            </View>
                            <Button mode="outlined" color="#3D7282" labelStyle={{fontSize: 12}} style={{borderRadius: 20, height: 25, justifyContent:"center"}} onPress={() => {
                                removeFriend({id: item.id, name: item.name, imageUrl: item.imageUrl});
                            }}>Remove</Button>
                        </View>
                    );
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(attendeesList);