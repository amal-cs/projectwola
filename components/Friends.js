import React, { useContext } from 'react';
import { View, StyleSheet, Text, FlatList, Image, Alert } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { Card, Button } from 'react-native-paper';
import { Context as HomeContext } from '../context/HomeContext';

const friends = () => {

    const { state } = useContext(HomeContext);

    const checkFriendList = () => {
        if (state.userData.friendList.length > 0) {
            return true;
        }
        return false;
    }

    const removeFriend = (uId) => {
        var friends = [];
        state.userData.friendList.map(friend => {
            if (friend.id != uId) {
                friends.push(friend.id);
            }
        });
        firestore().collection("Users").doc(uId).get().then(snapshot => {
            var friendsLis = snapshot.get("friendList");
            var newList = friendsLis.filter(friend => {
                return friend != state.userData.id;
            });
            firestore().collection("Users").doc(uId).update({
                friendList: newList
            });
        });
        firestore().collection("Users").doc(state.userData.id).update({
            friendList: friends
        });
    };

    return (
        <>
            {!checkFriendList() ? <Image source={require('../assets/null.png')} style={{ width: 250, height: 250, alignSelf: "center" }} /> :
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={state.userData.friendList}
                    keyExtractor={(item) => `${item.id}`}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.friendView}>
                                <View style={styles.innerFriendView}>
                                    <Card style={styles.friendCard}>
                                        <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                    </Card>
                                    <Text style={styles.friendText}>{item.name}</Text>
                                </View>
                                <Button mode="outlined" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                    Alert.alert(
                                        'Confirm',
                                        'Do you want to remove him from your friend list ?',
                                        [
                                            {
                                                text: 'Cancel',
                                                onPress: () => { console.log("cancel") },
                                                style: 'cancel'
                                            },
                                            {
                                                text: 'Yes',
                                                onPress: () => { removeFriend(item.id) },
                                            }
                                        ],
                                        { cancelable: true }
                                    );
                                }}>Friend</Button>
                            </View>
                        );
                    }}
                />
            }

        </>
    );
}

const styles = StyleSheet.create({
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(friends);