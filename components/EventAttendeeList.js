import React, { useContext } from 'react';
import { View, StyleSheet, Text, Image, FlatList, Alert } from 'react-native';
import { Card, Button } from 'react-native-paper';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as EventDetailContext } from '../context/EventDetailContext';
import { Context as EventListContext } from '../context/EventListContext';

const eventAttendeeList = ({ event,eventType }) => {

    const { state } = useContext(HomeContext);
    const { removeFromConfirm } = useContext(EventListContext);
    const { cancelInvitation } = useContext(EventDetailContext);

    return (
        <View>
            <FlatList
                data={event.attendees}
                keyExtractor={(item) => `${item.id}`}
                renderItem={({ item }) => {
                    const removeAdmin = () => {
                        if (item.id === state.userData.id) {
                            return true;
                        } else {
                            return false;
                        }
                    };
                    return (
                        <View style={styles.friendView}>
                            <View style={styles.innerFriendView}>
                                <Card style={styles.friendCard}>
                                    <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                </Card>
                                <Text style={styles.friendText}>{item.name}</Text>
                            </View>
                            {!removeAdmin() &&
                                <Button mode="contained" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                    Alert.alert(
                                        "Remove",
                                        "Do you want to remove him ?",
                                        [
                                            {
                                                text: "Cancel",
                                                onPress: () => {console.log("Cancelled")},
                                                style: "cancel"
                                            },
                                            {
                                                text: "Yes",
                                                onPress: () => {
                                                    removeFromConfirm(item.id,event.eventId,eventType);
                                                    cancelInvitation(item.id,event.eventId);
                                                },
                                                style: "default"
                                            }
                                        ],
                                        {cancelable: true}
                                    );
                                }}>Remove</Button>
                            }


                        </View>
                    )
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(eventAttendeeList);