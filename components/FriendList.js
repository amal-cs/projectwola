import React,{useContext} from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import { Card, Button } from 'react-native-paper';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as NewEventContext} from '../context/NewEventContext';

const friendList = () => {

    const {state} = useContext(HomeContext);
    const {state: eventState, addFriendList, sendInvite} = useContext(NewEventContext);
    React.useEffect(() => {
        addFriendList(state.userData.friendList);
    }, []);

    return (
        <View>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={eventState.searchFriendList}
                keyExtractor={(item) => `${item.id}`}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.friendView}>
                            <View style={styles.innerFriendView}>
                                <Card style={styles.friendCard}>
                                    <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                </Card>
                                <Text style={styles.friendText}>{item.name}</Text>
                            </View>
                            <Button mode="contained" color="#3D7282" labelStyle={{fontSize: 12}} style={{borderRadius: 20, height: 25, justifyContent:"center"}} onPress={() => {
                                sendInvite({id: item.id,name: item.name, imageUrl: item.imageUrl});
                            }}>Add</Button>
                        </View>
                    );
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(friendList);