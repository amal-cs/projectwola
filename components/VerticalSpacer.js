import React from 'react';
import { View, StyleSheet } from 'react-native';

const VerticalSpacer = ({children}) => {
    return(
        <View style={styles.spacer}>{children}</View>
    );
}

const styles = StyleSheet.create({
    spacer: {
        marginVertical: 50
    }
});

export default VerticalSpacer;