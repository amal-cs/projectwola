import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';

const settingsBlock = ({ image, mainText, subText, navigationTo, navScreen }) => {
    return (
        <TouchableOpacity onPress={() => navigationTo(navScreen)}>
            <View style={styles.blockContainer}>

                <View style={styles.content}>
                    <Image style={styles.image} source={image} />
                    <View style={styles.titleContents}>
                        <Text style={styles.mainText}>{mainText}</Text>
                        <Text style={styles.subText}>{subText}</Text>
                    </View>
                </View>
                <Image style={styles.arrow} source={require('../assets/next.png')} />

            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    blockContainer: {
        alignSelf: "stretch",
        height: 60,
        flexDirection: "row",
        backgroundColor: "#ECECEC",
        alignItems: "center",
        borderRadius: 10,
        padding: 8,
        justifyContent: "space-between",
        marginVertical: 5
    },
    content: {
        flexDirection: "row",
        alignItems: "center"
    },
    image: {
        width: 35,
        height: 35
    },
    arrow: {
        width: 20,
        height: 20
    },
    titleContents: {
        flexDirection: "column",
        marginLeft: 10
    },
    mainText: {
        fontSize: 18,
    },
    subText: {
        fontSize: 12,
        color: "#707070"
    }
});

export default settingsBlock;