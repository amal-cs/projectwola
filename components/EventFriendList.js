import React, { useContext } from 'react';
import { View, StyleSheet, Text, Image, FlatList, Alert } from 'react-native';
import { Card, Button } from 'react-native-paper';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as EventListContext } from '../context/EventListContext';
import { Context as EventDetailContext } from '../context/EventDetailContext';

const eventFriendList = ({ event,eventType }) => {

    const { state } = useContext(HomeContext);
    const { state: eventState, updatePending, addToPending, removeFromConfirm } = useContext(EventListContext);
    const { cancelPending, sendInvitation, cancelInvitation } = useContext(EventDetailContext);
    return (
        <View>
            <FlatList
                data={state.userData.friendList}
                keyExtractor={(item) => `${item.id}`}
                renderItem={({ item }) => {
                    const checkPendStatus = () => {
                        return event.pendAttendees.some((el) => {
                            return el === item.id;
                        });
                    }
                    const checkInviteStatus = () => {
                        return event.attendees.some((el) => {
                            return el.id === item.id;
                        });
                    }
                    return (
                        <View style={styles.friendView}>
                            <View style={styles.innerFriendView}>
                                <Card style={styles.friendCard}>
                                    <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                </Card>
                                <Text style={styles.friendText}>{item.name}</Text>
                            </View>
                            {checkPendStatus() &&
                                <Button mode="contained" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                    Alert.alert(
                                        "Remove",
                                        "Do you want to cancel request ?",
                                        [
                                            {
                                                text: "Cancel",
                                                onPress: () => {console.log("Cancel")},
                                                style: "cancel"
                                            },
                                            {
                                                text: "Yes",
                                                onPress: () => {
                                                    updatePending(item.id,event.eventId,eventType);
                                                    cancelPending(item.id,event.eventId);
                                                },
                                                style: "default"
                                            }
                                        ],
                                        {cancelable: true}
                                    );
                                }}>Pending</Button>
                            }
                            {!checkPendStatus() && !checkInviteStatus() &&
                                <Button mode="outlined" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                    Alert.alert(
                                        "Invite",
                                        "Do you want to send invitation ?",
                                        [
                                            {
                                                text: "Cancel",
                                                onPress: () => {console.log("Cancel")},
                                                style: "cancel"
                                            },
                                            {
                                                text: "Yes",
                                                onPress: () => {
                                                    addToPending(item.id,event.eventId,eventType);
                                                    sendInvitation(item.id,event.eventId);
                                                },
                                                style: "default"
                                            }
                                        ],
                                        {cancelable: true}
                                    );
                                }}>Invite</Button>
                            }
                            {!checkPendStatus() && checkInviteStatus() &&
                                <Button mode="outlined" color="#3D7282" labelStyle={{ fontSize: 12 }} style={{ borderRadius: 20, height: 25, justifyContent: "center" }} onPress={() => {
                                    Alert.alert(
                                        "Remove",
                                        "Do you want to remove him ?",
                                        [
                                            {
                                                text: "Cancel",
                                                onPress: () => {console.log("Cancel")},
                                                style: "cancel"
                                            },
                                            {
                                                text: "Yes",
                                                onPress: () => {
                                                    removeFromConfirm(item.id,event.eventId,eventType);
                                                    cancelInvitation(item.id,event.eventId);
                                                },
                                                style: "default"
                                            }
                                        ],
                                        {cancelable: true}
                                    );
                                }}>Confirmed</Button>
                            }

                        </View>
                    );
                }}
            /> 
        </View>
    );
}

const styles = StyleSheet.create({
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center"
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 16,
        color: "#3D7282",
        marginLeft: 10
    },
});

export default React.memo(eventFriendList);