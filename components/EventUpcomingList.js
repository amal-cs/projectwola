import React, { useContext, useState } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import { Card } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as EventListContext } from '../context/EventListContext';
import { navigate } from '../navigation/navigationRef';


const eventUpcomingList = () => {

    const { state, getUpcomingEvents, getMoreUpcomingEvents } = useContext(EventListContext);
    
    const { state: homeState } = useContext(HomeContext);


    const checkEmpty = () => {
        if (state.upcomingEvent.length === 0) {
            return false;
        } else {
            return true;
        }
    }

    const onRefresh = () => {
        getUpcomingEvents(homeState.userData.id);
    }

    const getMore = () => {
        getMoreUpcomingEvents(homeState.userData.id, state.upcomingLastId);
    }

    return (
        <>
            {state.isUpLoading ? (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/7774-loading.json')} autoPlay loop />) :
                <>
                    {checkEmpty() &&
                        <FlatGrid
                            showsVerticalScrollIndicator={false}
                            itemDimension={130}
                            onRefresh={() => onRefresh()}
                            refreshing={state.isUpLoading}
                            onEndReachedThreshold={0.5}
                            onEndReached={()=>{getMore()}}
                            items={state.upcomingEvent}
                            style={styles.container}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity onPress={() => navigate('EventDetailScreen', { id: item.eventId, eventType: "upcomingEvent" })}>
                                    <View style={styles.itemContainer}>

                                        <View>
                                            <View style={styles.header}>
                                                <Text style={styles.title}>{item.name}</Text>
                                                <Image style={styles.dotImage} source={require('../assets/dots.png')} />
                                            </View>
                                            <Text style={styles.subText} >{item.date}</Text>
                                        </View>
                                        <View >
                                            <View style={styles.participantStyle}>
                                                <Text style={{ fontSize: 12 }}>Participants</Text>
                                                <Text style={{ color: "#3D7282" }}>{item.attendees.length}</Text>
                                            </View>
                                            <FlatList
                                                horizontal
                                                style={{ marginTop: 3 }}
                                                data={item.attendees.slice(0, 4)}
                                                keyExtractor={(item) => `${item.id}`}
                                                renderItem={({ item: inItem, index: inIndex }) => {
                                                    if (inIndex === 0) {
                                                        return (
                                                            <Card style={styles.friendCard}>
                                                                <Image source={{ uri: inItem.imageUrl }} style={styles.friendImage} />
                                                            </Card>
                                                        );
                                                    } else {
                                                        return (
                                                            <Card style={styles.friendCard2}>
                                                                <Image source={{ uri: inItem.imageUrl }} style={styles.friendImage2} />
                                                            </Card>
                                                        );
                                                    }

                                                }}
                                            />
                                        </View>


                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                    }

                    {
                        !checkEmpty() &&
                        <View>
                            <Image source={require('../assets/null.png')} style={{ width: 250, height: 250, alignSelf: "center" }} />
                            <Text style={{ alignSelf: "center" }}>No upcoming events</Text>
                        </View>
                    }
                </>
            }
        </>


    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10
    },
    itemContainer: {
        flexDirection: "column",
        backgroundColor: "#fff",
        borderRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 10,
        height: 150,
        justifyContent: "space-between"
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    dotImage: {
        height: 15,
        width: 10,
    },
    title: {
        fontWeight: "bold",
        flexWrap: "wrap",
        fontSize: 18
    },
    subText: {
        color: "#707070",
        fontSize: 12,
        flexWrap: "wrap"
    },
    friendCard: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        borderWidth: 2,
        borderColor: "#000",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 19,
        height: 19,
        borderRadius: 9.5,
    },
    friendCard2: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        borderWidth: 2,
        borderColor: "#000",
        alignItems: "center",
        padding: 1,
        marginLeft: -5
    },
    friendImage2: {
        width: 19,
        height: 19,
        borderRadius: 9.5,
    },
    participantStyle: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    }
});

export default React.memo(eventUpcomingList);