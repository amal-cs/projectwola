import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const privacyPolicyScreen = () => {
    return(
        <View style={styles.container}>
            <Text>Privacy Screen</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

export default privacyPolicyScreen;