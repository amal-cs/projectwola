import React, { useContext, useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, TextInput, FlatList, Dimensions, TouchableOpacity, StatusBar } from 'react-native';
import { Card } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as EventListContext } from '../context/EventListContext';
import { Context as EventDetailContext } from '../context/EventDetailContext';
import EventFriendList from '../components/EventFriendList';
import EventAttendeeList from '../components/EventAttendeeList';
import Offline from '../components/Offline';

const initialLayout = { width: Dimensions.get('window').width };

const eventDetailScreen = ({ route, navigation }) => {

    const [network, setNetwork] = useState(true);
    const { id } = route.params;
    const { eventType } = route.params;
    const { state } = useContext(HomeContext);
    const { state: eventState } = useContext(EventListContext);
    const { state: eventDetState, setEvents } = useContext(EventDetailContext);
    const [loading, setLoading] = useState(false);
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Friends List' },
        { key: 'second', title: 'Confirmed List' },
    ]);

    useEffect(() => {
        const subscriber = navigation.addListener('focus', () => {
            setLoading(true);
            if (eventType === 'upcomingEvent') {
                var event = eventState.upcomingEvent.filter(evnt => {
                    return evnt.eventId === id;
                });
                setEvents(event[0]);
            } else if (eventType === 'todaysEvent') {
                var event = eventState.todaysEvent.filter(evnt => {
                    return evnt.eventId === id;
                });
                setEvents(event[0]);
            } else {
                var event = eventState.completedEvents.filter(evnt => {
                    return evnt.eventId === id;
                });
                setEvents(event[0]);
            }
            setLoading(false);
        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const renderscene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <EventFriendList event={eventDetState.event} eventType={eventType} />;
            case 'second':
                return <EventAttendeeList event={eventDetState.event} eventType={eventType} />;
            default:
                return null;
        }
    }

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: "#3D7282" }}
            style={{ backgroundColor: "#F2F2F2", elevation: 0 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color: "#000" }}>{route.title}</Text>
            )}
        />
    );

    const checkAdmin = () => {
        if (eventDetState.event.createdBy === state.userData.id) {
            return true;
        } else {
            return false;
        }
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate('EventScreen')}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Image style={styles.addImage} source={{ uri: state.userData.imageUrl }} />
                </View>
                <Text style={{ color: "#707070", fontSize: 16, marginTop: 12 }}>Hi, {state.userData.name}</Text>
                <Text style={styles.heading}>Event Details</Text>
                {loading && (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/7774-loading.json')} autoPlay loop />)}
                {!loading &&
                    <>
                        <Card style={styles.eventCard}>
                            <TextInput autoCapitalize="none" autoCorrect={false} editable={false} placeholder="Name your event" style={styles.inputStyle} value={eventDetState.event.name} />
                            <Text style={styles.subTextStyle}>Date and Time</Text>
                            <View style={styles.dateView}>
                                <Image style={styles.calenderImg} source={require('../assets/calenders.png')} />
                                <Text style={styles.dateStyle}>{eventDetState.event.date}</Text>
                            </View>
                            <View style={styles.timeView}>
                                <Image style={styles.timeImg} source={require('../assets/clocks.png')} />
                                <Text style={styles.dateStyle}>{eventDetState.event.time}</Text>
                            </View>
                        </Card>
                        {!checkAdmin() &&
                            <View style={{ marginTop: 8 }}>
                                <View>
                                    <View style={styles.line}></View>
                                    <Text style={styles.subHeaderText}>Invited Friends</Text>
                                </View>
                                <FlatList
                                    data={eventDetState.event.attendees}
                                    keyExtractor={(item) => `${item.id}`}
                                    renderItem={({ item }) => {
                                        return (
                                            <View style={styles.friendView}>
                                                <View style={styles.innerFriendView}>
                                                    <Card style={styles.friendCard}>
                                                        <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                                    </Card>
                                                    <Text style={styles.friendText}>{item.name}</Text>
                                                </View>
                                            </View>
                                        );
                                    }}
                                />
                            </View>
                        }
                        {checkAdmin() &&
                            <>
                                <View>
                                    <View style={styles.line}></View>
                                    <Text style={styles.subHeaderText}>Invite Friends</Text>
                                </View>
                                <TabView
                                    navigationState={{ index, routes }}
                                    renderScene={renderscene}
                                    onIndexChange={setIndex}
                                    initialLayout={initialLayout}
                                    renderTabBar={renderTabBar}
                                />
                            </>
                        }
                    </>
                }
            </View>

            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        marginHorizontal: 16,
        flex: 1
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 30,
        height: 30
    },
    addImage: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    eventCard: {
        padding: 14,
        borderRadius: 10,
        alignSelf: "stretch",
        marginTop: 12
    },
    inputStyle: {
        alignSelf: "stretch",
        height: 45,
        borderWidth: 1,
        borderColor: "#E7ECED",
        borderRadius: 10,
        paddingLeft: 10,
        marginBottom: 8
    },
    subTextStyle: {
        color: "#A9AEAF",
        fontSize: 12,
        marginBottom: 8
    },
    dateView: {
        flexDirection: 'row',
        alignItems: "center",
        marginBottom: 10
    },
    timeView: {
        flexDirection: 'row',
        alignItems: "center"
    },
    calenderImg: {
        width: 18,
        height: 18
    },
    timeImg: {
        width: 18,
        height: 18
    },
    dateStyle: {
        fontSize: 14,
        color: "#A9AEAF",
        marginLeft: 10
    },
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        alignItems: "center",
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 18,
        color: "#3D7282",
        marginLeft: 10
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginTop: 14
    },
    subHeaderText: {
        fontSize: 16,
        marginBottom: 10
    },
});

export default eventDetailScreen;