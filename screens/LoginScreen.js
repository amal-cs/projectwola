import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, Image, ScrollView, BackHandler, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { GoogleSigninButton, GoogleSignin } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import Offline from '../components/Offline';

const loginScreen = ({ navigation }) => {


    GoogleSignin.configure({
        webClientId: '580223493657-rth309fuet19li6lg7ehsaovhou55hme.apps.googleusercontent.com'
    });

    const [network, setNetwork] = useState(true);
    const [loading, setLoading] = useState(false);
    var backHandler = null;

    useEffect(() => {

        const backAction = () => {
            BackHandler.exitApp();
        };

        const subscriber = navigation.addListener("focus", () => {
            backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );
        });

        return subscriber;

    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            setLoading(false);
            backHandler.remove();
        });

        return subscriber;
    }, [navigation]);


    return (
        <>
            <LinearGradient colors={['#4CA0AE', '#438594', '#3B6D7D']} style={styles.linearGradient}>
                <StatusBar backgroundColor="#4CA0AE" barStyle="dark-content" />

                <Image source={require('../assets/logo.png')} style={styles.image} />
                <Text style={{ alignSelf: "center" }}>
                    <Text style={styles.title}>Wola</Text>
                    <Text style={styles.secondTitle}> App</Text>
                </Text>
                <View style={styles.subTitle}>
                    <Text style={{ color: "#fff", fontWeight: "bold" }}>Connect with people</Text>
                    <Text style={{ color: "#fff", fontWeight: "bold" }}>like never before</Text>
                </View>

                {loading ? (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/lf30_editor_Gd2qjs.json')} autoPlay loop />) :
                    <GoogleSigninButton
                        style={styles.googleSignin}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={() => {
                            setLoading(true);
                            signIn();
                        }} />
                }

            </LinearGradient>
            {network ? null : <Offline />}
        </>
    );
}


const signIn = async () => {
    try {
        const { idToken } = await GoogleSignin.signIn();
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        const user = auth().signInWithCredential(googleCredential);
    } catch (err) {
        console.log(err);
    }
}


const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    image: {
        height: 200,
        width: 200,
    },
    title: {
        fontSize: 40,
        color: "#fff",
        fontWeight: "bold"
    },
    secondTitle: {
        fontSize: 30,
        color: "#fff"
    },
    subTitle: {
        alignItems: "center",
        marginTop: 20,
    },
    googleSignin: {
        width: 200,
        height: 48,
        marginTop: 50
    }
});

export default loginScreen;