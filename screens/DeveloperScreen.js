import React from 'react';
import { View,StyleSheet, Text, Image, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const developerScreen = () => {
    return (
        <LinearGradient colors={['#4CA0AE', '#438594', '#3B6D7D']} style={styles.linearGradient}>
            <View style={{ flex:3,alignItems: "center", justifyContent:"center"}}>
            <StatusBar backgroundColor="#4CA0AE" barStyle="light-content" />
            <Image source={require('../assets/logo.png')} style={styles.image} />
            <Text>
                <Text style={{ fontSize: 35, color: "#fff", fontWeight: "bold" }}>Wola</Text>
                <Text style={{ fontSize: 25, color: "#fff" }}> App</Text>
            </Text>
            <Text style={{ fontSize: 14, color: "#fff" }}>Connect like never before</Text>
            <Text style={{ fontSize: 18, color: "#fff", marginTop: 18 }}>Share each moment</Text>
            <Text style={{ fontSize: 18, color: "#fff" }}>with your beloved ones</Text>
            </View>
            <View style={{flex: 1,flexDirection:"row", alignItems:"center",}}>
                <Text style={{color: "#fff"}}>Developed By</Text>
                <Text style={{color: "#fff", fontSize: 16, fontWeight: "bold"}}> Maxffort Technologies</Text>
            </View>
        </LinearGradient>
    );
};

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center"
    },
    image: {
        height: 200,
        width: 200
    }
});

export default developerScreen;