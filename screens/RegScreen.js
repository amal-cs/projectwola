import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, Text, TextInput, ScrollView, Image, TouchableOpacity, ToastAndroid, StatusBar } from 'react-native';
import { Checkbox } from 'react-native-paper';
import ImagePicker from 'react-native-image-crop-picker';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import { Context as AuthContext } from '../context/AuthContext';
import { Context as HomeContext } from '../context/HomeContext';
import { requestCameraAndStoragePermission } from '../helpers/Permissions';
import Offline from '../components/Offline';

const regScreen = ({ navigation, route }) => {

    const { type } = route.params;
    const [network, setNetwork] = useState(true);
    const [checkBox, setCheckBox] = useState(false);
    const [imageUri, setImageUri] = useState(null);
    const [imagePath, setImagePath] = useState(null);
    const [name, setName] = useState('');

    const { state, registerUser, updateUser } = useContext(AuthContext);
    const { state: homeState } = useContext(HomeContext);

    useEffect(() => {
        if (type === "update") {
            setImageUri(homeState.userData.imageUrl);
            setName(homeState.userData.name.charAt(0).toUpperCase() + homeState.userData.name.slice(1));
        }
    }, []);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const options = {
        title: 'Choose photo',
        storageOptions: {
            skipBackup: true,
            path: 'images'
        }
    }


    return (
        <>
            {state.isLoading && (<LottieView source={require('../assets/21462-loader-jumps.json')} autoPlay loop />)}
            {!state.isLoading && (
                <>
                    <View style={styles.container}>
                        <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                        <ScrollView>
                            <View>
                                <Text style={styles.title}>{(type === "update") ? "Profile" : "Registration"}</Text>
                                <Text style={styles.subtitle}>Let us know about yourself</Text>
                                <View style={styles.line}></View>
                                <Text style={styles.subText}>Upload Your Image</Text>
                                <TouchableOpacity onPress={async () => {

                                    ImagePicker.openPicker({
                                        width: 640,
                                        height: 640,
                                        cropping: true
                                    }).then(images => {
                                        setImagePath(images.path);
                                        setImageUri(images.path);
                                    }).catch(err => {
                                        console.log("Errors", err);
                                    });

                                }}>
                                    <Image style={styles.userImg} source={(imageUri == null) ? require('../assets/proimg.png') : { uri: imageUri }} />
                                </TouchableOpacity>
                                <View style={styles.line}></View>
                                <Text style={styles.subText}>Your Cool name</Text>
                                <TextInput autoCapitalize="none" autoCorrect={false} placeholder="Name" style={styles.textInput} value={name} onChangeText={setName} />
                                {(type === "update") ? null : <View style={styles.policyBox}>
                                    <Checkbox status={checkBox ? "checked" : "unchecked"}
                                        onPress={() => {
                                            setCheckBox(!checkBox);
                                        }}
                                    />
                                    <Text style={styles.policyText}>I agree to Privacy Policy and Terms and Conditions</Text>
                                </View>}
                                {(type === "update") ?
                                    <View style={styles.nextBox}>
                                        <TouchableOpacity onPress={() => {
                                            
                                            updateUser(name, imagePath, homeState.userData.id);
                                            
                                            ToastAndroid.show("Updated user", ToastAndroid.SHORT);
                                        }}>
                                            <View style={styles.nextButton}>
                                                <Text style={{ color: "#fff", fontSize: 14 }}>Update</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={styles.nextBox}>
                                        <TouchableOpacity onPress={() => {
                                            if (!checkBox) {
                                                ToastAndroid.show("Accept terms and conditions", ToastAndroid.SHORT);
                                            } else if (!imagePath) {
                                                ToastAndroid.show("Add your profile picture", ToastAndroid.SHORT);
                                            } else if (!name) {
                                                ToastAndroid.show("Add your name", ToastAndroid.SHORT);
                                            } else {
                                                registerUser(name, imagePath, state.user);
                                                
                                            }

                                        }}>
                                            <View style={styles.nextButton}>
                                                <Text style={{ color: "#fff", fontSize: 14 }}>Next</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }

                            </View>
                        </ScrollView>
                    </View>
                    {network ? null : <Offline />}
                </>
            )}
        </>
    );
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 16
    },
    title: {
        fontSize: 28,
        marginTop: 30,
        fontWeight: "bold"
    },
    subtitle: {
        fontSize: 14,
        color: '#808080',
        marginBottom: 40
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD"
    },
    subText: {
        fontSize: 16,
        marginBottom: 10
    },
    userImg: {
        width: 120,
        height: 120,
        marginBottom: 20,
        borderRadius: 50
    },
    textInput: {
        alignSelf: "stretch",
        height: 45,
        borderWidth: 1,
        borderColor: "#708090",
        borderRadius: 6,
        paddingLeft: 8,
    },
    policyBox: {
        marginTop: 40,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    policyText: {
        flex: 1,
        flexWrap: "wrap",
        fontSize: 14
    },
    nextBox: {
        alignSelf: "stretch",
        alignItems: "flex-end",
        marginTop: 25
    },
    nextButton: {
        backgroundColor: "#4B9FAD",
        alignItems: "center",
        justifyContent: "center",
        height: 40,
        width: 80,
        borderRadius: 50
    }
});

export default regScreen;