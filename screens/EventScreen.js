import React, { useContext, useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image, Dimensions, BackHandler, StatusBar, TouchableOpacity } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import RNShake from 'react-native-shake';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as EventListContext } from '../context/EventListContext';
import EventTodayList from '../components/EventTodayList';
import EventUpcomingList from '../components/EventUpcomingList';
import EventHistoryList from '../components/EventHistoryList';
import Offline from '../components/Offline';

const initialLayout = { width: Dimensions.get('window').width };

const eventScreen = ({ navigation }) => {

    const [network, setNetwork] = useState(true);
    const { state } = useContext(HomeContext);
    const { state: eventListState, getUpcomingEvents, getTodaysEvents, getCompletedEvents, getUpcomingCount } = useContext(EventListContext);

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Today' },
        { key: 'second', title: 'Upcoming' },
        { key: 'third', title: 'History' }
    ]);

    var backHandler = null;

    useEffect(() => {

        getUpcomingCount(state.userData.id);
        getUpcomingEvents(state.userData.id);
        getTodaysEvents(state.userData.id);
        getCompletedEvents(state.userData.id);


        const backAction = () => {
            BackHandler.exitApp();
        };

        const subscriber = navigation.addListener("focus", () => {
            backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );
        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            backHandler.remove();
        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    useEffect(() => {
        const subscriber = RNShake.addEventListener('ShakeEvent', () => {
            getUpcomingCount(state.userData.id);
            getUpcomingEvents(state.userData.id);
            getTodaysEvents(state.userData.id);
            getCompletedEvents(state.userData.id);
        });

        return subscriber;
    }, [])


    const renderscene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <EventTodayList />;
            case 'second':
                return <EventUpcomingList />;
            case 'third':
                return <EventHistoryList />;
            default:
                return null;
        }
    }

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: "#3D7282" }}
            style={{ backgroundColor: "#F2F2F2", elevation: 0 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color: "#000" }}>{route.title}</Text>
            )}
        />
    );


    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate("DeveloperScreen")}>
                        <View style={styles.lineGroup}>
                            <View style={styles.line}></View>
                            <View style={styles.middleLine}></View>
                            <View style={styles.line}></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Registration", { type: "update" })}>
                        <Image style={styles.imagePro} source={(state.userData.imageUrl.length > 0) ? { uri: state.userData.imageUrl } : require('../assets/proimg.png')} />
                    </TouchableOpacity>
                </View>

                <>
                    <Text style={styles.subText}>Hello, {state.userData.name.charAt(0).toUpperCase() + state.userData.name.slice(1)}</Text>
                    <Text style={styles.heading}>You have {eventListState.upcomingCount} {"\n"}Upcoming Events</Text>
                    <View style={styles.headLine}></View>
                    <Text style={styles.subHeaderText}>Your Events</Text>
                    <TabView
                        navigationState={{ index, routes }}
                        renderScene={renderscene}
                        onIndexChange={setIndex}
                        initialLayout={initialLayout}
                        renderTabBar={renderTabBar}
                    />
                </>

            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    middleLine: {
        height: 4,
        width: 28,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    lineGroup: {
        flexDirection: 'column'
    },
    imagePro: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    subText: {
        color: "#707070",
        marginTop: 16,
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 8
    },
    subHeaderText: {
        fontSize: 16,
        marginBottom: 14
    },
    headLine: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginTop: 14
    },
});

export default eventScreen;