import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, ImageBackground, Modal, StatusBar } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { FlatGrid } from 'react-native-super-grid';
import ImagePicker from 'react-native-image-crop-picker';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import { Context as AuthContext } from '../context/AuthContext';
import { Context as AlbumContext } from '../context/AlbumContext';
import Offline from '../components/Offline';

const photoCollectionScreen = ({ route, navigation }) => {

    const [network, setNetwork] = useState(true);
    const { state, getSelectedEvent, addImage, setAlbumLoading } = useContext(AlbumContext);
    const { state: authState } = useContext(AuthContext);
    const { id } = route.params;
    const [showModal, setShowModal] = useState(false);
    const [index, setIndex] = useState(0);
    

    useEffect(() => {

        const subscribe = navigation.addListener("focus", () => {
            setAlbumLoading();
            getSelectedEvent(id);
            
        });

        return subscribe;
    }, [navigation]);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            setAlbumLoading();
        });

        return subscriber;
    })


    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const uploadImage = () => {
        ImagePicker.openPicker({
            width: 1080,
            height: 640,
            cropping: true
        }).then(images => {
            addImage(images.path, state.selectEvent.eventId);
        }).catch(err => {
            console.log("Errors", err);
        });
    };

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor={showModal? "#000" :"#f2f2f2"} barStyle={showModal? "light-content" : "dark-content"}/>
                <Modal visible={showModal} transparent={true} onRequestClose={() => {setShowModal(false);}}>
                    <ImageViewer imageUrls={state.selectedAlbum} index={index} />
                </Modal>
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => { navigation.navigate('AlbumScreen') }}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Collections</Text>
                    <TouchableOpacity onPress={() => {
                        uploadImage();
                    }}>
                        <Image style={styles.addImage} source={require('../assets/addPlus.png')} />
                    </TouchableOpacity>
                </View>
                {state.isAlbumLoading && (<LottieView source={require('../assets/7861-loading-animation.json')} autoPlay loop />)}
                {!state.isAlbumLoading &&
                    <>
                        <Text style={styles.heading}>{state.selectEvent.name}</Text>
                        <Text style={styles.subHeading}>{state.selectEvent.date}</Text>
                        {state.selectEvent.album &&
                            <FlatGrid
                                itemDimension={130}
                                items={state.selectEvent.album}
                                style={{ marginTop: 8 }}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity onPress={() => {
                                        // navigation.navigate("PhotoScreen", { url: item });
                                        setIndex(index);
                                        setShowModal(true);
                                    }}>
                                        <View style={{ height: 100, backgroundColor: "#fff", borderRadius: 10 }}>
                                            <ImageBackground imageStyle={{ borderRadius: 10 }} source={{ uri: item }} style={styles.bgImage}>
                                            </ImageBackground>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        }

                        {!state.selectEvent.album &&
                            <Image source={require('../assets/null.png')} style={{ width: 250, height: 250, alignSelf: "center", marginTop: 50 }} />
                        }
                    </>
                }

            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 30,
        height: 30
    },
    addImage: {
        width: 25,
        height: 25,
    },
    headerText: {
        color: "#707070",
        fontSize: 16
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 12
    },
    subHeading: {
        color: "#707070",
    },
    bgImage: {
        flex: 1,
        resizeMode: "cover"
    }
});

export default photoCollectionScreen;