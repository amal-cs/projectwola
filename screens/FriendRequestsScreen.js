import React, { useContext, useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/functions';
import { Card, Button } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import Offline from '../components/Offline';

const friendRequestsScreen = ({ navigation }) => {

    const [network, setNetwork] = useState(true);
    const { state } = useContext(HomeContext);
    const [pendingFriends, setPendingFriends] = useState([]);

    useEffect(() => {
        const subscribe = navigation.addListener('focus', () => {
            fetchRequestData();
        });

        return subscribe;
    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const checkPending = () => {
        if (state.userData.friendReq.length === 0) {
            return true;
        } else {
            return false;
        }
    };

    const fetchRequestData = () => {
        if (state.userData.friendReq.length > 0) {
            firestore().collection("Users").where("id", "in", state.userData.friendReq).onSnapshot(documentSnapshot => {
                var arr = [];
                var count = 0;
                documentSnapshot.forEach(snapshot => {
                    arr.push(
                        {
                            id: snapshot.get("id"),
                            name: snapshot.get("name"),
                            imageUrl: snapshot.get("imageUrl")
                        }
                    );
                    count++;
                    if (count === documentSnapshot.size) {
                        setPendingFriends(arr);
                    }
                });
            });
        }
    }

    const acceptRequest = (uId) => {
        firestore().collection("Users").doc(state.userData.id).update({
            friendList: firestore.FieldValue.arrayUnion(uId),
            friendReq: firestore.FieldValue.arrayRemove(uId)
        }).then(() => {
            firestore().collection("Users").doc(uId).update({
                pendFriendReq: firestore.FieldValue.arrayRemove(state.userData.id),
                friendList: firestore.FieldValue.arrayUnion(state.userData.id)
            });

            firestore().collection("Users").doc(uId).get().then(snapshot => {
                var data = firebase.functions().httpsCallable('acceptFriendRequest');
                data({
                    tokens: snapshot.get("tokens"),
                    name: state.userData.name
                }).then(res => {
                    console.log(res);
                });
            });

        });
    }

    const rejectRequest = (uId) => {
        firestore().collection("Users").doc(state.userData.id).update({
            friendReq: firestore.FieldValue.arrayRemove(uId)
        }).then(() => {
            firestore().collection("Users").doc(uId).update({
                pendFriendReq: firestore.FieldValue.arrayRemove(state.userData.id)
            });
        });
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => { navigation.navigate('SettingsScreen') }}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Image style={styles.addImage} source={{ uri: state.userData.imageUrl }} />
                </View>
                <Text style={styles.heading}>Friend Requests</Text>
                <Text style={styles.subHeading}>{state.userData.friendReq.length} requests</Text>
                {checkPending() &&
                    <Image source={require('../assets/null.png')} style={{ alignSelf: "center", width: 250, height: 250 }} />
                }
                {!checkPending() &&

                    <FlatList
                        style={{ marginTop: 12 }}
                        data={pendingFriends}
                        keyExtractor={(item) => `${item.id}`}
                        renderItem={({ item }) => {
                            return (
                                <View style={styles.friendView}>
                                    <View style={styles.innerFriendView}>
                                        <Card style={styles.friendCard}>
                                            <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                        </Card>
                                        <View style={{ marginLeft: 14 }}>
                                            <Text style={styles.friendText}>{item.name.charAt(0).toUpperCase() + item.name.slice(1)}</Text>
                                            <Text style={{ fontSize: 12, color: "#707070" }}>send you a request</Text>
                                            <View style={{ flexDirection: "row", marginVertical: 5 }}>
                                                <Button color="#228B22" labelStyle={{ fontSize: 10 }} style={{ height: 25, justifyContent: "center", marginRight: 12 }} mode="outlined" onPress={() => acceptRequest(item.id)}>Accept</Button>
                                                <Button color="#FF6347" labelStyle={{ fontSize: 10 }} style={{ height: 25, justifyContent: "center" }} mode="outlined" onPress={() => rejectRequest(item.id)}>Reject</Button>
                                            </View>
                                        </View>
                                    </View>

                                </View>
                            );
                        }}
                    />

                }
            </View >
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 30,
        height: 30
    },
    addImage: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    headerText: {
        color: "#707070",
        fontSize: 16
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 12
    },
    subHeading: {
        color: "#707070"
    },
    friendView: {
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        marginVertical: 5,
        padding: 8,
        alignItems: "center",
        backgroundColor: "#fff",
        borderRadius: 10
    },
    innerFriendView: {
        flexDirection: "row",
        alignItems: "center"
    },
    friendCard: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
    },
    friendImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    friendText: {
        fontSize: 18,
        color: "#3D7282",
        flexWrap: "wrap",
    },
});

export default friendRequestsScreen;