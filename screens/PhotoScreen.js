import React, { useState } from 'react';
import { View, StyleSheet, Image, StatusBar } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';

const photoScreen = ({ route }) => {
    const { url, index } = route.params;
    const [picIndex, setPicIndex] = useState(index);
    const config = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80
    };

    const swipeRight = (gestState) => {
        setPicIndex(0);
    };

    const swipeLeft = (gestState) => {
        if(url.length > 1){
            setPicIndex(1);
        }
    };

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#000" barStyle="light-content" />
            <GestureRecognizer 
                config={config}
                onSwipeRight={(state) => swipeRight(state)}
                onSwipeLeft={(state) => swipeLeft(state)}
            >
                <Image source={{ uri: url[picIndex] }} style={styles.image} />
            </GestureRecognizer>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#000"
    },
    image: {
        alignSelf: "stretch",
        height: 250,
    }
});

export default photoScreen;