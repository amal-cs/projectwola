import React, { useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TextInput, Dimensions, TouchableOpacity, ToastAndroid, StatusBar } from 'react-native';
import { Card, TextInput as PaperTextInput } from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker'
import { TabView, TabBar } from 'react-native-tab-view';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchInput from 'react-native-search-filter';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import Moment from 'moment';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as NewEventContext } from '../context/NewEventContext';
import FriendList from '../components/FriendList';
import AttendeesList from '../components/AttendeesList';
import Offline from '../components/Offline';

const initialLayout = { width: Dimensions.get('window').width };

const eventCreateScreen = ({ navigation, route }) => {

    const [network, setNetwork] = useState(true);
    const { evName } = route.params;
    const { state } = useContext(HomeContext);
    const { state: eventState, clearAttendeeList, handleSearch, registerEvent, setLoading, unsetLoading } = useContext(NewEventContext);
    const [name, setName] = useState('');
    const [date, setDate] = useState(new Date(1598051730000));
    const [time, setTime] = useState(new Date(1598051730000));
    const [show, setShow] = useState(false);
    const [timeShow, setTimeShow] = useState(false);
    const [dateChange, setDateChange] = useState(false);
    const [timeChange, setTimeChange] = useState(false);
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Friends List' },
        { key: 'second', title: 'Invited List' },
    ]);
    Moment.locale('en');

    useEffect(() => {
        
        const unsubscribe = navigation.addListener('blur', () => {
            clearAttendeeList();
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const subscriber = navigation.addListener("focus", () => {
            unsetLoading();
        });

        return subscriber;
    }, [navigation])

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const renderscene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <FriendList />;
            case 'second':
                return <AttendeesList />;
            default:
                return null;
        }
    }

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: "#3D7282" }}
            style={{ backgroundColor: "#F2F2F2", elevation: 0 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color: "#000" }}>{route.title}</Text>
            )}
        />
    );

    const submitEvent = () => {

        if (name === '' || !dateChange || !timeChange) {
            ToastAndroid.show("Complete every fields", ToastAndroid.SHORT);
        } else {
            setLoading();
            var selDate = Moment(date).format("MMM Do YYYY");
            var selTime = Moment(time).format('HH:mm');
            registerEvent(name, selDate, selTime, state.userData.id, date, eventState.attendeeList);
        }
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Create Event</Text>
                    <TouchableOpacity onPress={() => {
                        submitEvent();
                    }}>
                        <Image style={styles.tickImage} source={require('../assets/tick.png')} />
                    </TouchableOpacity>
                </View>
                {eventState.isLoading && (<LottieView source={require('../assets/21462-loader-jumps.json')} autoPlay loop />)}
                {!eventState.isLoading && (
                    <>
                        <Text style={styles.subText}>Hello, {state.userData.name}</Text>
                        <Text style={styles.heading}>It's {"\n"}{evName} Time</Text>


                        <View style={styles.line}></View>
                        <Text style={styles.subHeaderText}>Details</Text>
                        <Card style={styles.eventCard}>
                            <TextInput autoCapitalize="none" autoCorrect={false} placeholder="Name your event" style={styles.inputStyle} value={name} onChangeText={setName} />
                            <Text style={styles.subTextStyle}>Date and Time</Text>
                            <View style={styles.dateView}>
                                <Image style={styles.calenderImg} source={require('../assets/calenders.png')} />
                                {(!dateChange) ? <Text onPress={() => { setShow(true); }} style={styles.dateStyle}>Select Date</Text> : <Text onPress={() => { setShow(true); }} style={styles.dateStyle}>{Moment(date).format('YYYY-MM-DD')}</Text>}
                                {show && <DateTimePicker
                                    value={date}
                                    mode="date"
                                    onChange={(event, newdate) => {
                                        setShow(false);
                                        var newdate = newdate || date;
                                        setDate(newdate);
                                        setDateChange(true);
                                    }}
                                />}

                            </View>
                            <View style={styles.timeView}>
                                <Image style={styles.timeImg} source={require('../assets/clocks.png')} />
                                {(!timeChange) ? <Text onPress={() => { setTimeShow(true); }} style={styles.dateStyle}>Select Time</Text> : <Text onPress={() => { setTimeShow(true); }} style={styles.dateStyle}>{Moment(time).format('HH:mm')}</Text>}
                                {timeShow && <DateTimePicker
                                    value={time}
                                    mode="time"
                                    is24Hour={true}
                                    onChange={(event, newtime) => {
                                        setTimeShow(false);
                                        var newtime = newtime || time;
                                        setTime(newtime);
                                        setTimeChange(true);
                                    }}
                                />}
                            </View>
                        </Card>

                        <View style={styles.searchView}>
                            <View>
                                <View style={styles.line}></View>
                                <Text style={styles.subHeaderText2}>Invite Friends</Text>
                            </View>
                            <View style={styles.searchSection}>
                                <Ionicons style={styles.searchIcon} name="ios-search" size={20} color="#A9AEAF" />
                                <SearchInput onChangeText={(searchTerm) => {
                                    handleSearch(searchTerm);
                                }} placeholder="Search" style={styles.searchInput} />
                            </View>
                        </View>

                        <TabView
                            navigationState={{ index, routes }}
                            renderScene={renderscene}
                            onIndexChange={setIndex}
                            initialLayout={initialLayout}
                            renderTabBar={renderTabBar}
                        />
                    </>
                )}

            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        marginHorizontal: 16,
        flex: 1,
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 50
    },
    searchIcon: {
        padding: 10,
    },
    searchView: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: 20
    },
    searchInput: {
        height: 40,
        width: 180,
        borderRadius: 50,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: "#fff"
    },
    arrow: {
        width: 30,
        height: 30
    },
    tickImage: {
        width: 25,
        height: 25,
    },
    headerText: {
        color: "#707070",
        fontSize: 16
    },
    subText: {
        color: "#707070",
        marginTop: 16,
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 8
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginTop: 14
    },
    subHeaderText: {
        fontSize: 16,
        marginBottom: 14
    },
    subHeaderText: {
        fontSize: 16,
        marginBottom: 8
    },
    eventCard: {
        alignSelf: "stretch",
        height: 160,
        paddingVertical: 10,
        paddingHorizontal: 14,
        borderRadius: 10
    },
    inputStyle: {
        alignSelf: "stretch",
        height: 45,
        borderWidth: 1,
        borderColor: "#E7ECED",
        borderRadius: 10,
        paddingLeft: 10,
        marginBottom: 8
    },
    dateView: {
        flexDirection: 'row',
        alignItems: "center",
        marginBottom: 10
    },
    timeView: {
        flexDirection: 'row',
        alignItems: "center"
    },
    calenderImg: {
        width: 18,
        height: 18
    },
    timeImg: {
        width: 18,
        height: 18
    },
    dateStyle: {
        fontSize: 14,
        color: "#A9AEAF",
        marginLeft: 10
    },
    subTextStyle: {
        color: "#A9AEAF",
        fontSize: 12,
        marginBottom: 8
    },
});

export default eventCreateScreen;