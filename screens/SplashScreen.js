import React, { useContext } from 'react';
import { StyleSheet, Image, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import auth from '@react-native-firebase/auth';
import { Context as AuthContext } from '../context/AuthContext';

const splashScreen = () => {

    const { onAuthStateChanged } = useContext(AuthContext);

    React.useEffect(() => {
        const subscriber = auth().onAuthStateChanged((user) => {
            onAuthStateChanged(user);
        });
        return subscriber;

    }, []);

    return (
        <LinearGradient colors={['#4CA0AE', '#438594', '#3B6D7D']} style={styles.linearGradient}>
            <StatusBar backgroundColor="#4CA0AE" barStyle="light-content"/>
            <Image source={require('../assets/logo.png')} style={styles.image} />
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    image: {
        height: 200,
        width: 200
    }
});

export default splashScreen;