import React, { useEffect } from 'react';
import { Image, Text, StyleSheet, View, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import auth from '@react-native-firebase/auth';

const signoutScreen = ({ navigation }) => {

    useEffect(() => {
        const subscriber = navigation.addListener("focus", () => {
            setTimeout(() => {
                auth()
                    .signOut()
                    .then(() => console.log('User signed out!'));
            }, 1000)
        });

        return subscriber;
    }, [navigation])


    return (
        <LinearGradient colors={['#4CA0AE', '#438594', '#3B6D7D']} style={styles.linearGradient}>
            <StatusBar backgroundColor="#4CA0AE" barStyle="light-content" />
            <Image source={require('../assets/logo.png')} style={styles.image} />
            <Text style={styles.text}>Signing you out...</Text>
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    image: {
        height: 200,
        width: 200
    },
    text: {
        color: "#fff",
    }
});

export default signoutScreen;