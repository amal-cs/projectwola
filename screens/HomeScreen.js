import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, Text, Image, FlatList, ImageBackground, TouchableOpacity, BackHandler, StatusBar } from 'react-native';
import { Card } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import { Context as AuthContext } from '../context/AuthContext';
import { Context as HomeContext } from '../context/HomeContext';
import Offline from '../components/Offline';

const homeScreen = ({ navigation }) => {

    const { state, getUserData } = useContext(HomeContext);
    const { state: authState } = useContext(AuthContext);
    const [network, setNetwork] = useState(true);

    const data1 = [
        {
            id: 'id-party',
            name: 'Party',
            image: require('../assets/party.jpg')
        },
        {
            id: 'id-meeting',
            name: 'Meeting',
            image: require('../assets/meeting.jpg')
        }
    ];

    const data2 = [
        {
            id: 'id-movie',
            name: 'Movie',
            image: require('../assets/movie.jpg')
        },
        {
            id: 'id-date',
            name: 'Date',
            image: require('../assets/date.jpg')
        }
    ];

    var backHandler = null;

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    useEffect(() => {
        messaging()
            .getToken()
            .then(token => {
                return saveTokenToDatabase(token);
            });

        return messaging().onTokenRefresh(token => {
            saveTokenToDatabase(token);
        });
    }, [])

    useEffect(() => {

        getUserData(authState.user);

        const backAction = () => {
            BackHandler.exitApp();
        }

        const subscriber = navigation.addListener("focus", () => {
            backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );
        });

        return subscriber;

    }, [navigation]);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            backHandler.remove();
        });

        return subscriber;
    }, [navigation]);

    const saveTokenToDatabase = async (token) => {
        const userId = auth().currentUser.uid;

        await firestore()
            .collection('Users')
            .doc(userId)
            .update({
                tokens: firestore.FieldValue.arrayUnion(token),
            });
    }


    const checkFriendList = () => {
        if (state.userData.friendList.length < 1) {
            return true;
        }
        return false;
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate("DeveloperScreen")}>
                        <View style={styles.lineGroup}>
                            <View style={styles.line}></View>
                            <View style={styles.middleLine}></View>
                            <View style={styles.line}></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Registration", { type: "update" })}>
                        <Image style={styles.imagePro} source={(state.userData.imageUrl.length > 0) ? { uri: state.userData.imageUrl } : require('../assets/proimg.png')} />
                    </TouchableOpacity>
                </View>
                {state.isLoading && (<LottieView source={require('../assets/7861-loading-animation.json')} autoPlay loop />)}
                {!state.isLoading &&
                    <>
                        <View style={{ flex: 2 }}>
                            <View style={{flex: 1, justifyContent: "center"}}>
                                <Text style={styles.headerText}>Hello{"\n"}{state.userData.name.charAt(0).toUpperCase() + state.userData.name.slice(1)}</Text>
                            </View>
                            <View style={{flex:3}}>
                                <View style={styles.subContainer}>
                                    <View style={styles.line}></View>
                                    <Text style={styles.subText}>Your Activities</Text>
                                </View>
                                <View>
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        style={styles.listStyle}
                                        data={data1}
                                        horizontal={true}
                                        keyExtractor={(item) => `${item.id}`}
                                        renderItem={({ item }) => {
                                            return (
                                                <Card style={styles.cardStyle} onPress={() => {
                                                    navigation.navigate('EventCreateScreen', { evName: item.name });
                                                }}>
                                                    <ImageBackground imageStyle={{ borderRadius: 10 }} source={item.image} style={styles.bgImage}>
                                                        <Text style={styles.cardText}>{item.name}</Text>
                                                    </ImageBackground>
                                                </Card>
                                            );
                                        }}
                                    />

                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        style={styles.listStyle}
                                        data={data2}
                                        horizontal={true}
                                        keyExtractor={(item) => `${item.id}`}
                                        renderItem={({ item }) => {
                                            return (
                                                <Card style={styles.cardStyle} onPress={() => {
                                                    navigation.navigate('EventCreateScreen', { evName: item.name });
                                                }}>
                                                    <ImageBackground imageStyle={{ borderRadius: 10 }} source={item.image} style={styles.bgImage}>
                                                        <Text style={styles.cardText}>{item.name}</Text>
                                                    </ImageBackground>
                                                </Card>
                                            );
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: "center" }}>
                            <View>
                                <View style={styles.friendListGroup}>
                                    <View style={styles.subContainer}>
                                        <View style={styles.line}></View>
                                        <Text style={styles.subText}>Friends List</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => {
                                        navigation.navigate("FriendListScreen");
                                    }}>
                                        <Text style={styles.link}>Find Friends</Text>
                                    </TouchableOpacity>
                                </View>
                                {checkFriendList() ? <LottieView style={{ width: 140, height: 100, alignSelf: "center" }} source={require('../assets/10223-search-empty.json')} autoPlay loop /> :
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        style={styles.listStyle1}
                                        data={state.userData.friendList.slice(0, 4)}
                                        horizontal={true}
                                        keyExtractor={(item) => `${item.id}`}
                                        renderItem={({ item }) => {
                                            return (
                                                <Card style={styles.friendCard}>
                                                    <Image source={{ uri: item.imageUrl }} style={styles.friendImage} />
                                                </Card>
                                            );
                                        }}
                                    />
                                }
                            </View>
                        </View>

                    </>
                }

            </View>
            {network ? null : <Offline />}

        </>
    );
}


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16,
        marginTop: 16,
        flex: 1,
        flexDirection: "column"
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    middleLine: {
        height: 4,
        width: 28,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    lineGroup: {
        flexDirection: 'column'
    },
    imagePro: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    headerText: {
        color: "#000",
        fontSize: 24,
        fontWeight: 'bold',
    },
    subContainer: {
        // marginTop: 18
    },
    subText: {
        fontWeight: 'bold'
    },
    listStyle: {
        marginTop: 14,
        alignSelf: "center"
    },
    listStyle1: {
        marginTop: 14,
    },
    cardStyle: {
        width: 158,
        height: 100,
        marginRight: 10,
        borderRadius: 10,
    },
    bgImage: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: 'flex-end',
        padding: 10
    },
    cardText: {
        color: "#fff",
        fontWeight: 'bold',
        alignSelf: 'flex-start'
    },
    friendListGroup: {
        flexDirection: 'row',
        alignItems: "flex-end",
        justifyContent: "space-between"
    },
    link: {
        fontSize: 13,
        color: "#707070",
    },
    friendCard: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 4,
        borderColor: "#3D7282",
        alignItems: "center",
        padding: 1,
        marginRight: 10
    },
    friendImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
    },

});

export default homeScreen;