import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, Text, Image, FlatList, ImageBackground, TouchableOpacity, BackHandler, StatusBar } from 'react-native';
import LottieView from 'lottie-react-native';
import RNShake from 'react-native-shake';
import NetInfo from '@react-native-community/netinfo';
import { FlatGrid } from 'react-native-super-grid';
import { Context as HomeContext } from '../context/HomeContext';
import { Context as AlbumContext } from '../context/AlbumContext';
import Offline from '../components/Offline';

const albumScreen = ({ navigation }) => {

    const [network, setNetwork] = useState(true);
    const { state } = useContext(HomeContext);
    const { state: albumState, getEventAlbum, getMoreEventAlbum } = useContext(AlbumContext);
    var pics = [];


    var backHandler = null;

    useEffect(() => {

        getEventAlbum(state.userData.id);

        const backAction = () => {
            BackHandler.exitApp();
        };

        const subscriber = navigation.addListener("focus", () => {
            backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );

        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            backHandler.remove();
        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = RNShake.addEventListener('ShakeEvent', () => {
            getEventAlbum(state.userData.id);
        });

        return subscriber;
    }, [])

    const onRefresh = () => {
        getEventAlbum(state.userData.id);
    }

    const getMore = () => {
        getMoreEventAlbum(state.userData.id, albumState.lastEventId);
    }

    const checkEmpty = () => {
        if (albumState.eventAlbum.length < 1) {
            return true;
        }
        return false;
    }



    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate("DeveloperScreen")}>
                        <View style={styles.lineGroup}>
                            <View style={styles.line}></View>
                            <View style={styles.middleLine}></View>
                            <View style={styles.line}></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Registration", { type: "update" })}>
                        <Image style={styles.imagePro} source={(state.userData.imageUrl.length > 0) ? { uri: state.userData.imageUrl } : require('../assets/proimg.png')} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.subText}>Hello, {state.userData.name.charAt(0).toUpperCase() + state.userData.name.slice(1)}</Text>
                <Text style={styles.heading}>Your{"\n"}Collection</Text>
                <View style={styles.headLine}></View>
                <Text style={styles.subHeaderText}>Your Photos</Text>

                {albumState.isLoading && (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/7774-loading.json')} autoPlay loop />)}
                {!albumState.isLoading &&
                    <>
                        {checkEmpty() ? <Image source={require('../assets/null.png')} style={{ width: 250, height: 250, alignSelf: "center" }} /> :
                            <FlatList
                                onRefresh={() => onRefresh()}
                                refreshing={albumState.isLoading}
                                onEndReached={() => { getMore() }}
                                onEndReachedThreshold={0.5}
                                showsVerticalScrollIndicator={false}
                                data={albumState.eventAlbum}
                                keyExtractor={(item) => `${item.eventId}`}
                                renderItem={({ item }) => {
                                    return (
                                        <View style={styles.cardContainer}>
                                            <View style={styles.cardHeader}>
                                                <Image source={require('../assets/dot.png')} style={{ width: 15, height: 15 }} />
                                                <View style={styles.cardSubHeader}>
                                                    <Text style={{ fontWeight: "bold", fontSize: 16 }}>{item.name}</Text>
                                                    <Text style={{ fontSize: 12, color: '#707070' }}>{item.date}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={styles.verticalLine}></View>
                                                <View style={{ flexDirection: "column", flexGrow: 3 }}>
                                                    {item.album &&
                                                        <FlatGrid
                                                            showsVerticalScrollIndicator={false}
                                                            itemDimension={120}
                                                            staticDimension={320}
                                                            items={item.album.slice(0, 2)}
                                                            renderItem={({ item: gridItem, index }) => (
                                                                <TouchableOpacity onPress={() => {
                                                                    navigation.navigate("PhotoScreen", { url: item.album.slice(0, 2), index: index });
                                                                }}>
                                                                    <View style={{ height: 100, backgroundColor: "#fff", borderRadius: 10 }}>
                                                                        <ImageBackground imageStyle={{ borderRadius: 10 }} source={{ uri: gridItem }} style={styles.bgImage}>
                                                                        </ImageBackground>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            )}
                                                        />
                                                    }
                                                    {!item.album &&
                                                        <Image source={require('../assets/null.png')} style={{ width: 150, height: 150, alignSelf: "center" }} />
                                                    }

                                                    <TouchableOpacity onPress={() => {
                                                        navigation.navigate("PhotoCollectionScreen", { id: item.eventId });
                                                    }}>
                                                        <Text style={{ alignSelf: "flex-end", fontSize: 12, color: "#707070" }}>More</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                        </View>
                                    );
                                }}
                            />
                        }
                    </>


                }


            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    middleLine: {
        height: 4,
        width: 28,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    verticalLine: {
        width: 3,
        alignSelf: "stretch",
        backgroundColor: "#4B9FAD",
        marginLeft: 5
    },
    lineGroup: {
        flexDirection: 'column'
    },
    imagePro: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    subText: {
        color: "#707070",
        marginTop: 16,
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 8
    },
    subHeaderText: {
        fontSize: 16,
        marginBottom: 14
    },
    headLine: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginTop: 14
    },
    cardHeader: {
        flexDirection: "row",
        alignItems: "center"
    },
    cardSubHeader: {
        flexDirection: 'column',
        marginHorizontal: 10
    },
    cardContainer: {
    },
    bgImage: {
        flex: 1,
        resizeMode: "cover"
    }
});

export default albumScreen;