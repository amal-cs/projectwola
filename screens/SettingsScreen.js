import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, Text, Image, ScrollView, BackHandler, StatusBar, TouchableOpacity } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import SettingsBlock from '../components/SettingsBlock';
import Offline from '../components/Offline';

const settingsScreen = ({ navigation }) => {

    const { state } = useContext(HomeContext);
    const [network, setNetwork] = useState(true);

    var backHandler = null;

    useEffect(() => {

        const subscriber = navigation.addListener("focus", () => {
            backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                () => BackHandler.exitApp()
            );
        });

        return subscriber;

    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    useEffect(() => {
        const subscriber = navigation.addListener("blur", () => {
            backHandler.remove();
        });

        return subscriber;
    }, [navigation]);



    const navigateTo = (screen) => {
        navigation.navigate(screen);
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate("DeveloperScreen")}>
                        <View style={styles.lineGroup}>
                            <View style={styles.line}></View>
                            <View style={styles.middleLine}></View>
                            <View style={styles.line}></View>
                        </View>
                    </TouchableOpacity>
                    {/* <Image style={styles.imagePro} source={require('../assets/proimg.png')} /> */}
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.profileHeader}>
                        <Image source={{ uri: state.userData.imageUrl }} style={styles.profileImage} />
                        <Text style={styles.profileName}>{state.userData.name.charAt(0).toUpperCase() + state.userData.name.slice(1)}</Text>
                    </View>
                    <View style={styles.generalContent}>
                        <View style={styles.line}></View>
                        <Text>General</Text>
                        <SettingsBlock navigationTo={(screen) => navigateTo(screen)} image={require('../assets/email.png')} mainText="Invitations" subText="Your pending invitations" navScreen="EventRequestsScreen" />
                        <SettingsBlock navigationTo={(screen) => navigateTo(screen)} image={require('../assets/friend.png')} mainText="Friend Requests" subText="Your pending requests" navScreen="FriendRequestsScreen" />
                        <SettingsBlock navigationTo={(screen) => navigateTo(screen)} image={require('../assets/logout.png')} mainText="Sign Out" subText="Sign out from the fun" navScreen="SignoutScreen" />
                    </View>
                    <View style={styles.generalContent}>
                        <View style={styles.line}></View>
                        <Text>Privacy</Text>
                        <SettingsBlock navigationTo={(screen) => navigateTo(screen)} image={require('../assets/privacy.png')} mainText="Privacy Policy" subText="Privacy policy of the app" navScreen="PrivacyPolicyScreen" />
                        <SettingsBlock navigationTo={(screen) => navigateTo(screen)} image={require('../assets/terms.png')} mainText="Terms and Conditions" subText="Terms and conditions of the app" navScreen="PrivacyPolicyScreen" />
                    </View>
                </ScrollView>
            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    line: {
        height: 4,
        width: 20,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    middleLine: {
        height: 4,
        width: 28,
        backgroundColor: "#4B9FAD",
        marginBottom: 3
    },
    imagePro: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    profileHeader: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 16
    },
    profileImage: {
        width: 110,
        height: 110,
        borderRadius: 55
    },
    profileName: {
        fontSize: 20,
        marginTop: 8
    },
    generalContent: {
        marginTop: 12
    }
});

export default settingsScreen;