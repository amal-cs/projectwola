import React, { useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, StatusBar } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import Friends from '../components/Friends';
import FindFriends from '../components/FindFriends';
import Offline from '../components/Offline';

const initialLayout = { width: Dimensions.get('window').width };

const friendListScreen = ({ navigation }) => {

    const { state } = useContext(HomeContext);
    const [network, setNetwork] = useState(true);
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Friends' },
        { key: 'second', title: 'Find Friends' },
    ]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const renderscene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <Friends />;
            case 'second':
                return <FindFriends />;
            default:
                return null;
        }
    }

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: "#3D7282" }}
            style={{ backgroundColor: "#F2F2F2", elevation: 0 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color: "#000" }}>{route.title}</Text>
            )}
        />
    );

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Image style={styles.addImage} source={{ uri: state.userData.imageUrl }} />
                </View>
                <Text style={styles.heading}>Your Friends</Text>
                <Text style={styles.subHeading}>{state.userData.friendList.length} friends</Text>
                <TabView
                    navigationState={{ index, routes }}
                    renderScene={renderscene}
                    onIndexChange={setIndex}
                    initialLayout={initialLayout}
                    renderTabBar={renderTabBar}
                />
            </View>
            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        marginHorizontal: 16,
        flex: 1
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 30,
        height: 30
    },
    addImage: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    headerText: {
        color: "#707070",
        fontSize: 16
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 12
    },
    subHeading: {
        color: "#707070"
    }
});

export default friendListScreen;