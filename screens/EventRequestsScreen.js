import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, Text, Image, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/functions';
import { Card, Button } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import NetInfo from '@react-native-community/netinfo';
import { Context as HomeContext } from '../context/HomeContext';
import Offline from '../components/Offline';

const eventRequestsScreen = ({ navigation }) => {

    const [network, setNetwork] = useState(true);
    const { state } = useContext(HomeContext);
    const [eventReqs, setEventReqs] = useState([]);
    const [loading, setLoading] = useState(false);
    const [emptyEvent, setEmptyEvent] = useState(false);

    useEffect(() => {
        const subscriber = navigation.addListener('focus', () => {
            fetchEvents();
        });

        return subscriber;
    }, [navigation]);

    useEffect(() => {
        const subscriber = NetInfo.addEventListener(state => {
            setNetwork(state.isConnected);
        });

        return subscriber;
    }, []);

    const fetchEvents = () => {
        setLoading(true);
        setEventReqs([]);
        firestore().collection("Users").doc(state.userData.id).onSnapshot(documentSnapshot => {
            if (documentSnapshot.get("eventRequests")) {
                if (documentSnapshot.get("eventRequests").length > 0) {
                    var eventReqIds = documentSnapshot.get("eventRequests");
                    var events = [];
                    while (eventReqIds.length > 0) {
                        var event10 = eventReqIds.slice(0, 10);
                        firestore().collection("Events").where("eventId", "in", event10).get().then(snapshot => {
                            var count = 0;
                            snapshot.forEach(event => {
                                events.push({
                                    eventId: event.get("eventId"),
                                    name: event.get("name"),
                                    date: event.get("date"),
                                    time: event.get("time"),
                                    status: event.get("status"),
                                    createdBy: event.get("createdBy")
                                });
                                count++;
                                if (count === snapshot.size) {
                                    setLoading(false);
                                    setEventReqs([...eventReqs, ...events]);
                                }
                            });
                        });
                        eventReqIds.splice(0, 10);
                    }

                } else {
                    setLoading(false);
                    setEmptyEvent(true);
                }
            } else {
                setLoading(false);
                setEmptyEvent(true);
            }
        });
    }

    const acceptRequest = (eventId, status, senderId, receiptName) => {
        setLoading(true);
        firestore().collection("Users").doc(state.userData.id).update({
            eventRequests: firestore.FieldValue.arrayRemove(eventId)
        }).then(() => {
            firestore().collection("Users").doc(state.userData.id).collection("events").doc(eventId).set({
                id: eventId,
                status: status
            });
            firestore().collection("Events").doc(eventId).update({
                attendeeListIds: firestore.FieldValue.arrayRemove(state.userData.id),
                confirmedListIds: firestore.FieldValue.arrayUnion(state.userData.id)
            }).then(() => {
                var newEventReqs = eventReqs.filter(event => {
                    return event.eventId != eventId;
                });
                setLoading(false);
                if (newEventReqs.length === 0) {
                    setEmptyEvent(true);
                }
                setEventReqs(newEventReqs);
            });
            firestore().collection("Users").doc(senderId).get().then(snapshot => {
                var data = firebase.functions().httpsCallable('acceptEventRequest');
                data({
                    tokens: snapshot.get("tokens"),
                    name: receiptName
                }).then(res => {
                    console.log(res);
                });
            });
        });
    }

    const rejectRequest = (eventId) => {
        setLoading(true);
        firestore().collection("Users").doc(state.userData.id).update({
            eventRequests: firestore.FieldValue.arrayRemove(eventId)
        }).then(() => {
            firestore().collection("Events").doc(eventId).update({
                attendeeListIds: firestore.FieldValue.arrayRemove(state.userData.id)
            }).then(() => {
                var newEventReqs = eventReqs.filter(event => {
                    return event.eventId != eventId;
                });
                setLoading(false);
                if (newEventReqs.length === 0) {
                    setEmptyEvent(true);
                }
                setEventReqs(newEventReqs);
            });
        });
    }

    return (
        <>
            <View style={styles.container}>
                <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
                <View style={styles.topBar}>
                    <TouchableOpacity onPress={() => { navigation.navigate('SettingsScreen') }}>
                        <Image style={styles.arrow} source={require('../assets/left-arrow.png')} />
                    </TouchableOpacity>
                    <Image style={styles.addImage} source={{ uri: state.userData.imageUrl }} />
                </View>
                <Text style={styles.heading}>Event Requests</Text>
                <Text style={styles.subHeading}>{eventReqs.length} requests</Text>
                {loading && (<LottieView style={{ width: 100, height: 100, alignSelf: "center" }} source={require('../assets/7774-loading.json')} autoPlay loop />)}
                {emptyEvent && !loading &&
                    <Image source={require('../assets/null.png')} style={{ alignSelf: "center", width: 250, height: 250 }} />
                }
                {!emptyEvent && !loading &&
                    <FlatList
                        style={{ marginTop: 12 }}
                        data={eventReqs}
                        keyExtractor={(item) => `${item.eventId}`}
                        renderItem={({ item }) => {
                            var owner = state.userData.friendList.filter(friend => {
                                return friend.id === item.createdBy;
                            });
                            return (
                                <Card style={styles.eventCard}>
                                    <View>
                                        <Text style={styles.cardHeading}>{item.name}</Text>
                                        <Text style={styles.cardSubHeading}>{item.time}</Text>
                                        <Text style={styles.cardSubHeading}>{item.date}</Text>
                                    </View>
                                    <View style={{ flexDirection: "row", alignSelf: "stretch", justifyContent: "space-between", marginTop: 8 }}>
                                        <Button color="#228B22" labelStyle={{ fontSize: 10 }} style={{ height: 35, justifyContent: "center", marginRight: 12 }} mode="outlined" onPress={() => acceptRequest(item.eventId, item.status, owner[0].id, state.userData.name)} >Accept</Button>
                                        <Button color="#FF6347" labelStyle={{ fontSize: 10 }} style={{ height: 35, justifyContent: "center", marginRight: 12 }} mode="outlined" onPress={() => rejectRequest(item.eventId)} >Reject</Button>
                                    </View>
                                    <View style={{ flexDirection: "row", alignSelf: "stretch", justifyContent: "flex-end", marginTop: 8 }}>
                                        <View style={{ flexDirection: "column" }}>
                                            <Text style={{ color: "#707070", flexWrap: "wrap" }}>By, {owner[0].name}</Text>
                                        </View>
                                    </View>

                                </Card>
                            );
                        }}
                    />
                }

            </View>

            {network ? null : <Offline />}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        marginHorizontal: 16
    },
    topBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    arrow: {
        width: 30,
        height: 30
    },
    addImage: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    headerText: {
        color: "#707070",
        fontSize: 16
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 12
    },
    subHeading: {
        color: "#707070"
    },
    eventCard: {
        padding: 12
    },
    cardHeading: {
        fontSize: 16,
        fontWeight: "bold"
    },
    cardSubHeading: {
        color: "#707070",
        fontSize: 12
    }
});

export default eventRequestsScreen;