import { PERMISSIONS, checkMultiple, requestMultiple } from 'react-native-permissions';

export async function checkCameraAndStoragePermission(){
    await checkMultiple([PERMISSIONS.ANDROID.CAMERA,PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]).then((statuses) => {
        
        if(statuses[PERMISSIONS.ANDROID.CAMERA] == 'denied' || statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] == 'denied' || statuses[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE] == 'denied'){
            return false;
        }else{
            return true;
        }
    });
}

export async function requestCameraAndStoragePermission(){
    await requestMultiple([PERMISSIONS.ANDROID.CAMERA,PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]).then((statuses) => {
        
        if(statuses[PERMISSIONS.ANDROID.CAMERA] == 'denied' || statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] == 'denied' || statuses[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE] == 'denied'){
            return false;
        }else{
            return true;
        }
    });
}