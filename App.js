import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigationRef } from './navigation/navigationRef';
import LoginScreen from './screens/LoginScreen';
import SplashScreen from './screens/SplashScreen';
import PhotoScreen from './screens/PhotoScreen';
import HomeScreen from './screens/HomeScreen';
import PhotoCollectionScreen from './screens/PhotoCollectionScreen';
import { Provider as AuthProvider } from './context/AuthContext';
import { Provider as HomeProvider } from './context/HomeContext';
import { Provider as NewEventProvider } from './context/NewEventContext';
import { Provider as EventListProvider } from './context/EventListContext';
import { Provider as AlbumProvider } from './context/AlbumContext';
import { Provider as EventDetailProvider } from './context/EventDetailContext';
import RegScreen from './screens/RegScreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import EventScreen from './screens/EventScreen';
import AlbumScreen from './screens/AlbumScreen';
import SettingsScreen from './screens/SettingsScreen';
import EventCreateScreen from './screens/EventCreateScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FriendListScreen from './screens/FriendListScreen';
import FriendRequestsScreen from './screens/FriendRequestsScreen';
import EventRequestsScreen from './screens/EventRequestsScreen';
import EventDetailScreen from './screens/EventDetailScreen';
import SignoutScreen from './screens/SignoutScreen';
import PrivacyPolicyScreen from './screens/PrivacyPolicyScreen';
import DeveloperScreen from './screens/DeveloperScreen';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();
const EventStack = createStackNavigator();

function EventStackNavigator() {
  return (
    <EventStack.Navigator>
      <EventStack.Screen name="HomeScreenContent" component={BottomTabNavigator} options={{ headerShown: false }} />
      <EventStack.Screen name="EventCreateScreen" component={EventCreateScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="PhotoScreen" component={PhotoScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="PhotoCollectionScreen" component={PhotoCollectionScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="FriendListScreen" component={FriendListScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="FriendRequestsScreen" component={FriendRequestsScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="EventRequestsScreen" component={EventRequestsScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="EventDetailScreen" component={EventDetailScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="SignoutScreen" component={SignoutScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="PrivacyPolicyScreen" component={PrivacyPolicyScreen} options={{ headerShown: false }} />
      <EventStack.Screen name="DeveloperScreen" component={DeveloperScreen} options={{ headerShown: false }} />
    </EventStack.Navigator>
  );
}

function BottomTabNavigator() {
  return (
    <BottomTab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'HomeScreen') {
            iconName = focused
              ? 'ios-home'
              : 'ios-home';
          } else if (route.name === 'EventScreen') {
            iconName = focused ? 'ios-calendar' : 'md-calendar';
          } else if (route.name === 'AlbumScreen') {
            iconName = focused ? 'ios-images' : 'ios-images';
          } else if (route.name === 'SettingsScreen') {
            iconName = focused ? 'ios-settings' : 'ios-settings';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}
    >
      <BottomTab.Screen name="HomeScreen" component={HomeScreen} options={{
        tabBarLabel: 'Home'
      }} />
      <BottomTab.Screen name="EventScreen" component={EventScreen} options={{
        tabBarLabel: 'Events'
      }} />
      <BottomTab.Screen name="AlbumScreen" component={AlbumScreen} options={{
        tabBarLabel: 'Albums'
      }} />
      <BottomTab.Screen name="SettingsScreen" component={SettingsScreen} options={{
        tabBarLabel: 'Settings'
      }} />
    </BottomTab.Navigator>
  );
}

function App() {

  

  return (
    <AuthProvider>
      <HomeProvider>
        <NewEventProvider>
          <EventListProvider>
            <AlbumProvider>
              <EventDetailProvider>
                <NavigationContainer ref={navigationRef}>
                  <Stack.Navigator>
                    <Stack.Screen name="Splash" component={SplashScreen} options={{
                      headerShown: false
                    }} />
                    <Stack.Screen name="Login" component={LoginScreen} options={{
                      headerShown: false
                    }} />
                    <Stack.Screen name="Registration" component={RegScreen} options={{
                      headerShown: false
                    }} />
                    <Stack.Screen name="Home" component={EventStackNavigator} options={{
                      headerShown: false
                    }} />
                  </Stack.Navigator>
                </NavigationContainer>
              </EventDetailProvider>
            </AlbumProvider>
          </EventListProvider>
        </NewEventProvider>
      </HomeProvider>
    </AuthProvider>
  );
}

export default App;