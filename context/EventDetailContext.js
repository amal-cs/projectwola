import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/functions';
import createDataContext from './createDataContext';

const eventDetailReducer = (state,action) => {
    switch(action.type){
        case 'set_event':
            return { ...state, event: action.payload };
        case 'cancel_pending':
            var pending = state.event.pendAttendees.filter(attendee => {
                return attendee != action.payload;
            });
            return { ...state, event: {...state.event,pendAttendees: pending}};
        case 'send_inv':
            if(state.event.pendAttendees){
                var newPend =  state.event.pendAttendees;
                newPend.push(action.payload);
                return {...state, event: {...state.event, pendAttendees: newPend}};
            }else{
                var newPend = [];
                newPend.push(action.payload);
                return {...state, event: {...state.event, pendAttendees: newPend}};
            }
        case 'cancel_inv':
            var confirmed = state.event.attendees.filter(friend => {
                return friend.id != action.payload;
            });
            return { ...state, event: {...state.event, attendees: confirmed}};
        default:
            return state;
    }
}

const setEvents = (dispatch) => {
    return (event) => {
        dispatch({type: 'set_event', payload: event});
    }
}

const cancelPending = (dispatch) => {
    return (uId, evntId) => {
        firestore().collection("Events").doc(evntId).update({
            attendeeListIds: firestore.FieldValue.arrayRemove(uId)
        }).then(() => {
            firestore().collection("Users").doc(uId).update({
                eventRequests: firestore.FieldValue.arrayRemove(evntId)
            }).then(() => {
                dispatch({type: 'cancel_pending',payload: uId})
            });
            
        });
        
    }
}

const sendInvitation = (dispatch) => {
    return (uId, evntId) => {
        firestore().collection("Events").doc(evntId).update({
            attendeeListIds: firestore.FieldValue.arrayUnion(uId)
        }).then(() => {
            firestore().collection("Users").doc(uId).get().then(snapshot => {
                var data = firebase.functions().httpsCallable('sendEventRequest');
                data({
                    tokens: snapshot.get("tokens")
                }).then(res => {
                    console.log(res);
                });
            });
            firestore().collection("Users").doc(uId).update({
                eventRequests: firestore.FieldValue.arrayUnion(evntId)
            }).then(() => {
                dispatch({type: 'send_inv', payload: uId});
            });
        });
        
    }
}

const cancelInvitation = (dispatch) => {
    return (uId, evntId) => {
        firestore().collection("Events").doc(evntId).update({
            confirmedListIds: firestore.FieldValue.arrayRemove(uId)
        }).then(() => {
            firestore().collection("Users").doc(uId).update({
                eventIds: firestore.FieldValue.arrayRemove(evntId)
            }).then(() => {
                dispatch({type: "cancel_inv", payload: uId});
            })
        });
        
    }
}

export const { Provider, Context } = createDataContext(
    eventDetailReducer,
    {setEvents, cancelPending, sendInvitation, cancelInvitation},
    {event: {name: "", date: "", time: "", serverTimeStamp: null, status: "", eventId: "", createdBy: "", attendees: [], pendAttendees: []}}
);