import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/functions';
import SearchInput, { createFilter } from 'react-native-search-filter';
import createDataContext from './createDataContext';
import { navigate } from '../navigation/navigationRef';

const KEYS_TO_FILTERS = ['name'];

const newEventReducer = (state,action) => {
    switch(action.type){
        case 'init_friend':
            return {...state, friendList: action.payload, searchFriendList: action.payload};
        case 'add_friend':
            var arr = state.friendList;
            var newArr = arr.filter((friend) => {
                return friend.id !== action.payload.id;
            });
            return {...state, friendList: newArr, attendeeList: [...state.attendeeList,action.payload], searchFriendList: newArr};
        case 'clear_attendee':
            return {...state, attendeeList: []};
        case 'search':
            const filteredFriends = state.friendList.filter(createFilter(action.payload, KEYS_TO_FILTERS));
            return {...state, searchFriendList: filteredFriends};
        case 'remove_friend':
            var arr = state.attendeeList;
            var newArr = arr.filter((friend) => {
                return friend.id !== action.payload.id;
            });
            return {...state, friendList: [...state.friendList,action.payload], attendeeList: newArr, searchFriendList: [...state.friendList,action.payload]};
        case 'success_reg':
            return {...state, isLoading: false};
        case 'set_loading':
            return {...state, isLoading: true};
        default: 
            return state;
    }
}

const addFriendList = (dispatch) => {
    return (list) => {
        dispatch({type: 'init_friend', payload: list});
    }
}

const sendInvite = (dispatch) => {
    return (selectedFriend) => {
        dispatch({type: 'add_friend', payload: selectedFriend});
    }
}

const clearAttendeeList = (dispatch) => {
    return () => {
        dispatch({type: 'clear_attendee'});
    }
}

const handleSearch = (dispatch) => {
    return (searchTerm) => {
        dispatch({type: 'search', payload: searchTerm});
    }
}

const removeFriend = (dispatch) => {
    return (selectedFriend) => {
        dispatch({type: 'remove_friend', payload: selectedFriend});
    }
}

const unsetLoading = (dispatch) => {
    return () => {
        dispatch({type: 'success_reg'});
    }
}

const registerEvent = (dispatch) => {
    return (name,date,time,userId,dateTimestamp,attendList) => {
        var confirmedListIds = [];
        var attendeeListIds = [];
        confirmedListIds.push(userId);
        attendList.map((data) => {
            attendeeListIds.push(data.id);
        });
        firestore().collection("Events")
        .add({
            name: name,
            date: date,
            time: time,
            createdBy: userId,
            dateTimestamp: dateTimestamp,
            attendeeListIds: attendeeListIds,
            confirmedListIds: confirmedListIds,
            serverTimeStamp: firestore.FieldValue.serverTimestamp()
        })
        .then(async ref => {
            var status = 'Pending';
            if(new Date(dateTimestamp) < new Date()){
                status = 'Completed';
            }
            await ref.set({eventId: ref.id, status: status}, {merge: true});
            await firestore().collection("Users").doc(userId).collection("events").doc(ref.id).set({
                id: ref.id,
                status: status
            });
            attendeeListIds.map((id) => {
                firestore().collection("Users").doc(id).update({
                    eventRequests: firestore.FieldValue.arrayUnion(ref.id)
                });
                firestore().collection("Users").doc(id).get().then(snapshot => {
                    var data = firebase.functions().httpsCallable('sendEventRequest');
                    data({
                        tokens: snapshot.get("tokens")
                    }).then(res => {
                        console.log(res);
                    });
                });
            });
        })
        .then(() => {
            // dispatch({type: "success_reg"});
            navigate('HomeScreenContent');
        });
    }
}

const setLoading = (dispatch) => {
    return () => {
        dispatch({type: "set_loading"});
    }
}

export const { Provider, Context } = createDataContext(
    newEventReducer,
    {addFriendList,sendInvite,clearAttendeeList,handleSearch,removeFriend,registerEvent,setLoading,unsetLoading},
    {friendList: [], attendeeList: [], searchFriendList: [], isLoading: false}
);