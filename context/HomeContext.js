import firestore from '@react-native-firebase/firestore';
import createDataContext from './createDataContext';


const homeReducer = (state, action) => {
    switch (action.type) {
        case 'add_user_data':
            return { userData: action.payload, isLoading: false };
        case 'set_loading':
            return { ...state, isLoading: true };
        default:
            return state;
    }
}

const getUserData = (dispatch) => {
    return (uId) => {
        dispatch({ type: 'set_loading' });
        firestore().collection("Users").doc(uId).onSnapshot(documentSnapshot => {
            if (documentSnapshot.get('friendList')) {
                var friendList = documentSnapshot.get('friendList');
                var data = {};
                if (documentSnapshot.get('pendFriendReq')) {
                    if (documentSnapshot.get('friendReq')) {
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: documentSnapshot.get('pendFriendReq'), friendReq: documentSnapshot.get('friendReq') };
                    } else {
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: documentSnapshot.get('pendFriendReq'), friendReq: []};
                    }
                } else {
                    if (documentSnapshot.get('friendReq')) {
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: [], friendReq: documentSnapshot.get('friendReq') };
                    } else {
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: [], friendReq: []};
                    }
                    
                }
                if(friendList.length > 0){
                    var frndCount = friendList.length;
                    var count = 0;
                    while(friendList.length > 0){
                        var friends10 = friendList.slice(0,10);
                        firestore().collection("Users").where("id", "in", friends10).get().then(querySnapshot => {
                            querySnapshot.forEach(snapshot => {
                                data.friendList.push(snapshot.data());
                                count++;
                            });
                            if(frndCount === count){
                                dispatch({ type: 'add_user_data', payload: data });
                            }
                            
                        });
                        friendList.splice(0,10);
                    }
                    
                }else{
                    dispatch({ type: 'add_user_data', payload: data });
                }
                
            } else {
                var data = {};
                if (documentSnapshot.get('pendFriendReq')) {
                    if(documentSnapshot.get('friendReq')){
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: documentSnapshot.get('pendFriendReq'), friendReq: documentSnapshot.get('friendReq')};
                    }else{
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: documentSnapshot.get('pendFriendReq'), friendReq: []};
                    }
                    
                } else {
                    if(documentSnapshot.get('friendReq')){
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: [], friendReq: documentSnapshot.get('friendReq') };
                    }else{
                        data = { id: uId, name: documentSnapshot.get('name'), imageUrl: documentSnapshot.get('imageUrl'), friendList: [], pendFriendReq: [], friendReq: [] };
                    }
                    
                }
                dispatch({ type: 'add_user_data', payload: data })
            }
        });
    }

}


export const { Provider, Context } = createDataContext(
    homeReducer,
    { getUserData },
    { userData: { id: '', name: '', imageUrl: '', friendList: [], pendFriendReq: [], friendReq: [] }, isLoading: false }
);