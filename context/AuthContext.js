import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import createDataContext from './createDataContext';
import { navigate } from '../navigation/navigationRef';

const authReducer = (state, action) => {
    switch (action.type) {
        case 'update_user':
            return { user: action.payload };
        case 'register_user':
            return { ...state, isRegistered: true, isLoading: false };
        case 'error_msg':
            return { ...state, errorMessage: action.payload, isLoading: false };
        case 'set_loading':
            return { ...state, isLoading: true};
        case 'unset_loading':
            return { ...state, isLoading: false};
        default:
            return state;
    }
}

const onAuthStateChanged = (dispatch) => {
    return async (user) => {
        if (user) {
            const { uid } = user;
            dispatch({ type: 'update_user', payload: uid });
            const documentSnapshot = await firestore().collection("Users").doc(uid).get();
            if (documentSnapshot.exists) {
                navigate('Home');
            } else {
                navigate('Registration',{type: "reg"});
            }
        } else {
            navigate('Login');
        }
    }
}

const registerUser = (dispatch) => {
    return async (name, imagePath, userId) => {
        try {
            dispatch({type: 'set_loading'});
            var lowName = name.toLowerCase();
            var imageRef = firebase.storage().ref("ProfileImages").child(userId + ".jpeg");
            imageRef.putFile(imagePath).then(() => {
                firebase.storage().ref(`ProfileImages/${userId}.jpeg`).getDownloadURL().then(url => {
                    firestore().collection("Users").doc(userId)
                        .set({
                            id: userId,
                            name: lowName,
                            imageUrl: url
                        }).then(() => {
                            dispatch({ type: 'register_user' });
                            navigate('Home');
                        }).catch(error => {
                            dispatch({ type: 'error_msg', payload: error });
                        });
                });
            }).catch(error => {
                dispatch({ type: 'error_msg', payload: error });
            });
        } catch (error) {
            dispatch({ type: 'error_msg', payload: error });
        }

    }
}

const updateUser = (dispatch) => {
    return async (name, imagePath, userId) => {
        dispatch({type: 'set_loading'});
        var lowName = name.toLowerCase();
        if(imagePath){
            var imageRef = firebase.storage().ref("ProfileImages").child(userId + ".jpeg");
            await imageRef.putFile(imagePath);
            const url = await firebase.storage().ref(`ProfileImages/${userId}.jpeg`).getDownloadURL();
            await firestore().collection("Users").doc(userId).update({
                name: lowName,
                imageUrl: url
            });
            dispatch({type: 'unset_loading'});
        }else{
            await firestore().collection("Users").doc(userId).update({
                name: lowName
            });
            dispatch({type: 'unset_loading'});
        }
    }
}


export const { Provider, Context } = createDataContext(
    authReducer,
    { onAuthStateChanged, registerUser, updateUser },
    { user: null, isRegistered: false, errorMessage: '', isLoading: false }
);