import firestore from '@react-native-firebase/firestore';
import createDataContext from './createDataContext';
import Moment from 'moment';

const eventListReducer = (state, action) => {
    switch (action.type) {
        case 'add_upcoming':
            var curDate = new Date();
            curDate = Moment(curDate).format("MMM Do YYYY");
            var arr = action.payload.arr.filter(data => {
                return data.date != curDate;
            });
            return { ...state, upcomingEvent: arr, isUpLoading: false, upcomingLastId: action.payload.lastEventId };
        case 'add_moreUpcoming':
            var curDate = new Date();
            curDate = Moment(curDate).format("MMM Do YYYY");
            var arr = action.payload.arr.filter(data => {
                return data.date != curDate;
            });
            return { ...state, upcomingEvent: [...state.upcomingEvent, ...arr], upcomingLastId: action.payload.lastEventId };
        case 'upcoming_count':
            return { ...state, upcomingCount: action.payload};
        case 'add_today':
            return { ...state, todaysEvent: [...action.payload, ...state.todaysEvent], isTodayLoading: false };
        case 'clear_today':
            return { ...state, todaysEvent: [] };
        case 'add_completed':
            return { ...state, completedEvents: action.payload.arr, isCompLoading: false, completedLastId: action.payload.lastEventId };
        case 'add_moreCompleted':
            return { ...state, completedEvents: [...state.completedEvents, ...action.payload.arr], completedLastId: action.payload.lastEventId };
        case 'set_loading':
            return { ...state, isCompLoading: true, isTodayLoading: true, isUpLoading: true };
        case 'unset_loading':
            return { ...state, isCompLoading: false, isTodayLoading: false, isUpLoading: false };
        case 'set_upLoading':
            return { ...state, isUpLoading: true };
        case 'unset_upLoading':
            return { ...state, isUpLoading: false };
        case 'set_toLoading':
            return { ...state, isTodayLoading: true };
        case 'unset_toLoading':
            return { ...state, isTodayLoading: false };
        case 'set_comLoading':
            return { ...state, isCompLoading: true };
        case 'unset_comLoading':
            return { ...state, isCompLoading: false };
        case 'update_pending':
            var evntType = action.payload.eventType;
            var uId = action.payload.uId;
            var evntId = action.payload.eventId;
            var events = [];
            switch (evntType) {
                case 'upcomingEvent':
                    state.upcomingEvent.map(event => {
                        if (event.eventId === evntId) {
                            var newPend = event.pendAttendees.filter(uid => {
                                return uid != uId;
                            });
                            events.push({ ...event, pendAttendees: newPend });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, upcomingEvent: events };
                case 'todaysEvent':
                    state.todaysEvent.map(event => {
                        if (event.eventId === evntId) {
                            var newPend = event.pendAttendees.filter(uid => {
                                return uid != uId;
                            });
                            events.push({ ...event, pendAttendees: newPend });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, todaysEvent: events };
                case 'completedEvents':
                    state.completedEvents.map(event => {
                        if (event.eventId === evntId) {
                            var newPend = event.pendAttendees.filter(uid => {
                                return uid != uId;
                            });
                            events.push({ ...event, pendAttendees: newPend });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, completedEvents: events };
                default:
                    return state;
            }
        case 'addto_pending':
            var evntType = action.payload.eventType;
            var uId = action.payload.uId;
            var evntId = action.payload.eventId;
            var events = [];
            switch (evntType) {
                case 'upcomingEvent':
                    state.upcomingEvent.map(event => {
                        if (event.eventId === evntId) {
                            if (event.pendAttendees) {
                                event.pendAttendees.push(uId);
                            } else {
                                event.pendAttendees = [uId];
                            }
                            events.push(event);
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, upcomingEvent: events };
                case 'todaysEvent':
                    state.todaysEvent.map(event => {
                        if (event.eventId === evntId) {
                            if (event.pendAttendees) {
                                event.pendAttendees.push(uId);
                            } else {
                                event.pendAttendees = [uId];
                            }
                            events.push(event);
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, todaysEvent: events };
                case 'completedEvents':
                    state.completedEvents.map(event => {
                        if (event.eventId === evntId) {
                            if (event.pendAttendees) {
                                event.pendAttendees.push(uId);
                            } else {
                                event.pendAttendees = [uId];
                            }
                            events.push(event);
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, completedEvents: events };
                default:
                    return state;
            }
        case 'remove_confirm':
            var evntType = action.payload.eventType;
            var uId = action.payload.uId;
            var evntId = action.payload.eventId;
            var events = [];
            switch (evntType) {
                case 'upcomingEvent':
                    state.upcomingEvent.map(event => {
                        if (event.eventId === evntId) {
                            var newAttnd = event.attendees.filter(friend => {
                                return friend.id != uId;
                            });
                            events.push({ ...event, attendees: newAttnd });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, upcomingEvent: events };
                case 'todaysEvent':
                    state.todaysEvent.map(event => {
                        if (event.eventId === evntId) {
                            var newAttnd = event.attendees.filter(friend => {
                                return friend.id != uId;
                            });
                            events.push({ ...event, attendees: newAttnd });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, todaysEvent: events };
                case 'completedEvents':
                    state.completedEvents.map(event => {
                        if (event.eventId === evntId) {
                            var newAttnd = event.attendees.filter(friend => {
                                return friend.id != uId;
                            });
                            events.push({ ...event, attendees: newAttnd });
                        } else {
                            events.push(event);
                        }

                    });
                    return { ...state, completedEvents: events };
                default:
                    return state;
            }
        default:
            return state;
    }
}

const getUpcomingCount = (dispatch) => {
    return (uId) => {
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Pending").get().then(documentSnapshot => {
            var count = documentSnapshot.size;
            dispatch({ type: 'upcoming_count', payload: count });
        });
    }
}

const getMoreUpcomingEvents = (dispatch) => {
    return (uId, lastId) => {
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Pending").orderBy("id").startAfter(lastId).limit(10).get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            await firestore().collection("Users").where("id", "in", dataSnapshot.get("confirmedListIds")).get().then(userData => {
                                arr.push(
                                    {
                                        eventId: dataSnapshot.get("eventId"),
                                        name: dataSnapshot.get("name"),
                                        date: dataSnapshot.get("date"),
                                        time: dataSnapshot.get("time"),
                                        status: dataSnapshot.get("status"),
                                        createdBy: dataSnapshot.get("createdBy"),
                                        dateTimestamp: dataSnapshot.get("dateTimestamp"),
                                        serverTimeStamp: dataSnapshot.get("serverTimeStamp"),
                                        attendees: [],
                                        pendAttendees: dataSnapshot.get("attendeeListIds")
                                    }
                                );
                                userData.forEach(user => {
                                    arr[count].attendees.push(
                                        {
                                            id: user.get("id"),
                                            name: user.get("name"),
                                            imageUrl: user.get("imageUrl"),
                                        }
                                    );
                                });
                                count++;
                                if (count == snapshot.size) {
                                    dispatch({ type: 'add_moreUpcoming', payload: { arr, lastEventId } });
                                }

                            });
                        });
                    } else {
                        dispatch({ type: 'unset_upLoading' });
                    }
                });

            } else {
                dispatch({ type: 'unset_upLoading' });
            }
        });

    }
}

const getUpcomingEvents = (dispatch) => {
    return (uId) => {
        dispatch({ type: 'set_upLoading' });

        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Pending").orderBy("id").limit(10).get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            await firestore().collection("Users").where("id", "in", dataSnapshot.get("confirmedListIds")).get().then(userData => {
                                arr.push(
                                    {
                                        eventId: dataSnapshot.get("eventId"),
                                        name: dataSnapshot.get("name"),
                                        date: dataSnapshot.get("date"),
                                        time: dataSnapshot.get("time"),
                                        status: dataSnapshot.get("status"),
                                        createdBy: dataSnapshot.get("createdBy"),
                                        dateTimestamp: dataSnapshot.get("dateTimestamp"),
                                        serverTimeStamp: dataSnapshot.get("serverTimeStamp"),
                                        attendees: [],
                                        pendAttendees: dataSnapshot.get("attendeeListIds")
                                    }
                                );
                                userData.forEach(user => {
                                    arr[count].attendees.push(
                                        {
                                            id: user.get("id"),
                                            name: user.get("name"),
                                            imageUrl: user.get("imageUrl"),
                                        }
                                    );
                                });
                                count++;
                                if (count == snapshot.size) {
                                    dispatch({ type: 'add_upcoming', payload: { arr, lastEventId } });
                                }

                            });
                        });
                    } else {
                        dispatch({ type: 'unset_upLoading' });
                    }
                });

            } else {
                dispatch({ type: 'unset_upLoading' });
            }
        });

    }
}

const getTodaysEvents = (dispatch) => {
    return (uId) => {
        dispatch({ type: 'set_toLoading' });
        dispatch({ type: "clear_today" });
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Pending").orderBy("id").get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var curDate = new Date();
                curDate = Moment(curDate).format("MMM Do YYYY");

                while (events.length > 0) {
                    var events10 = events.slice(0, 10);
                    firestore().collection("Events").where("eventId", "in", events10).where("date", "==", curDate).get().then(dataSnapshot => {
                        var arr = [];
                        var count = 0;
                        if (dataSnapshot.size > 0) {
                            dataSnapshot.forEach(async data => {
                                await firestore().collection("Users").where("id", "in", data.get("confirmedListIds")).get().then(snapshot => {
                                    arr.push(
                                        {
                                            eventId: data.get("eventId"),
                                            name: data.get("name"),
                                            date: data.get("date"),
                                            time: data.get("time"),
                                            status: data.get("status"),
                                            createdBy: data.get("createdBy"),
                                            dateTimestamp: data.get("dateTimestamp"),
                                            serverTimeStamp: data.get("serverTimeStamp"),
                                            attendees: [],
                                            pendAttendees: data.get("attendeeListIds")
                                        }
                                    );
                                    snapshot.forEach(user => {
                                        arr[count].attendees.push(
                                            {
                                                id: user.get("id"),
                                                name: user.get("name"),
                                                imageUrl: user.get("imageUrl"),
                                            }
                                        );
                                    });
                                    count++;
                                    if (count == dataSnapshot.size) {
                                        dispatch({ type: 'add_today', payload: arr });
                                    }
                                });
                            });
                        } else {
                            dispatch({ type: 'unset_toLoading' });
                        }
                    });
                    events.splice(0, 10);
                }

            } else {
                dispatch({ type: 'unset_toLoading' });
            }

        });
    }
}

const getMoreCompletedEvents = (dispatch) => {
    return (uId, lastId) => {
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Completed").orderBy("id").startAfter(lastId).limit(10).get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            await firestore().collection("Users").where("id", "in", dataSnapshot.get("confirmedListIds")).get().then(userData => {
                                arr.push(
                                    {
                                        eventId: dataSnapshot.get("eventId"),
                                        name: dataSnapshot.get("name"),
                                        date: dataSnapshot.get("date"),
                                        time: dataSnapshot.get("time"),
                                        status: dataSnapshot.get("status"),
                                        createdBy: dataSnapshot.get("createdBy"),
                                        dateTimestamp: dataSnapshot.get("dateTimestamp"),
                                        serverTimeStamp: dataSnapshot.get("serverTimeStamp"),
                                        attendees: [],
                                        pendAttendees: dataSnapshot.get("attendeeListIds")
                                    }
                                );
                                userData.forEach(user => {
                                    arr[count].attendees.push(
                                        {
                                            id: user.get("id"),
                                            name: user.get("name"),
                                            imageUrl: user.get("imageUrl"),
                                        }
                                    );
                                });
                                count++;
                                if (count == snapshot.size) {
                                    dispatch({ type: 'add_moreCompleted', payload: { arr, lastEventId } });
                                }

                            });
                        });
                    }

                });

            }
        });
    }
}

const getCompletedEvents = (dispatch) => {
    return (uId) => {
        dispatch({ type: 'set_comLoading' });

        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Completed").orderBy("id").limit(10).get().then(documentSnapshot => {

            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            await firestore().collection("Users").where("id", "in", dataSnapshot.get("confirmedListIds")).get().then(userData => {
                                arr.push(
                                    {
                                        eventId: dataSnapshot.get("eventId"),
                                        name: dataSnapshot.get("name"),
                                        date: dataSnapshot.get("date"),
                                        time: dataSnapshot.get("time"),
                                        status: dataSnapshot.get("status"),
                                        createdBy: dataSnapshot.get("createdBy"),
                                        dateTimestamp: dataSnapshot.get("dateTimestamp"),
                                        serverTimeStamp: dataSnapshot.get("serverTimeStamp"),
                                        attendees: [],
                                        pendAttendees: dataSnapshot.get("attendeeListIds")
                                    }
                                );
                                userData.forEach(user => {
                                    arr[count].attendees.push(
                                        {
                                            id: user.get("id"),
                                            name: user.get("name"),
                                            imageUrl: user.get("imageUrl"),
                                        }
                                    );
                                });
                                count++;
                                if (count == snapshot.size) {
                                    dispatch({ type: 'add_completed', payload: { arr, lastEventId } });
                                }

                            });
                        });
                    } else {
                        dispatch({ type: 'unset_comLoading' });
                    }

                });

            } else {
                dispatch({ type: "unset_comLoading" });
            }
        });


    }
}

const updatePending = (dispatch) => {
    return (uId, eventId, eventType) => {
        dispatch({ type: 'update_pending', payload: { uId, eventId, eventType } });
    }
}

const addToPending = (dispatch) => {
    return (uId, eventId, eventType) => {
        dispatch({ type: 'addto_pending', payload: { uId, eventId, eventType } })
    }
}

const removeFromConfirm = (dispatch) => {
    return (uId, eventId, eventType) => {
        dispatch({ type: 'remove_confirm', payload: { uId, eventId, eventType } })
    }
}

export const { Provider, Context } = createDataContext(
    eventListReducer,
    { getUpcomingEvents, getTodaysEvents, getCompletedEvents, updatePending, addToPending, removeFromConfirm, getMoreCompletedEvents, getMoreUpcomingEvents, getUpcomingCount },
    { upcomingCount: 0, upcomingEvent: [], todaysEvent: [], completedEvents: [], isUpLoading: false, isTodayLoading: false, isCompLoading: false, completedLastId: '', upcomingLastId: '' }
);