import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import createDataContext from './createDataContext';

const albumReducer = (state, action) => {
    switch (action.type) {
        case 'add_album':
            return { ...state, eventAlbum: action.payload.arr, isLoading: false, isAlbumLoading: false, lastEventId: action.payload.lastEventId };
        case 'add_moreAlbum':
            return { ...state, eventAlbum: [...state.eventAlbum, ...action.payload.arr], lastEventId: action.payload.lastEventId };
        case 'select_event':
            var event = state.eventAlbum.filter(events => {
                return events.eventId == action.payload;
            });
            var selectAlbum = [];
            if(event[0].album){
                if(event[0].album.length > 0){
                    event[0].album.map(pic => {
                        selectAlbum.push({url: pic});
                    });
                }
            }
            return { ...state, selectEvent: event[0], isLoading: false, isAlbumLoading: false, selectedAlbum: selectAlbum };
        case 'update_album':
            var selectedEvent = state.selectEvent;
            var selAlbum = state.selectedAlbum;
            if (selectedEvent.album) {
                selectedEvent.album.push(action.payload.url);
                selAlbum.push({url: action.payload.url});
            } else {
                selectedEvent.album = [];
                selectedEvent.album.push(action.payload.url);
                selAlbum.push({url: action.payload.url});
            }
            return { ...state, selectEvent: selectedEvent, isLoading: false, isAlbumLoading: false, selectedAlbum: selAlbum };
        case 'set_loading':
            return { ...state, isLoading: true };
        case 'set_album_loading':
            return { ...state, isAlbumLoading: true};
        case 'unset_loading':
            return { ...state, isLoading: false, isAlbumLoading: false };
        case 'set_albumloading':
            return { ...state, isAlbumLoading: true };
        default:
            return state;
    }
}

const setAlbumLoading = (dispatch) => {
    return () =>{
        dispatch({type: "set_album_loading"});
    }
}

const getMoreEventAlbum = (dispatch) => {
    return (uId, lastId) => {
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Completed").orderBy("id").startAfter(lastId).limit(10).get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).orderBy("dateTimestamp", "desc").get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            arr.push({
                                eventId: dataSnapshot.get("eventId"),
                                name: dataSnapshot.get("name"),
                                date: dataSnapshot.get("date"),
                                album: dataSnapshot.get("album")
                            });
                            count++;
                            if (count === snapshot.size) {
                                dispatch({ type: 'add_moreAlbum', payload: { arr, lastEventId } });
                            }
                        });
                    } else {
                        dispatch({ type: 'unset_loading' });
                    }

                });


            } else {
                dispatch({ type: 'unset_loading' });
            }

        });
    }
}

const getEventAlbum = (dispatch) => {
    return (uId) => {
        dispatch({ type: 'set_loading' })
        firestore().collection("Users").doc(uId).collection("events").where("status", "==", "Completed").orderBy("id").limit(10).get().then(documentSnapshot => {
            if (documentSnapshot.size > 0) {
                var events = [];
                documentSnapshot.forEach(dataSnapshot => {
                    events.push(dataSnapshot.get("id"));
                });
                var lastEventId = events[events.length - 1];
                firestore().collection("Events").where("eventId", "in", events).orderBy("dateTimestamp", "desc").get().then(snapshot => {
                    var arr = [];
                    var count = 0;
                    if (snapshot.size > 0) {
                        snapshot.forEach(async dataSnapshot => {
                            arr.push({
                                eventId: dataSnapshot.get("eventId"),
                                name: dataSnapshot.get("name"),
                                date: dataSnapshot.get("date"),
                                album: dataSnapshot.get("album")
                            });
                            count++;
                            if (count === snapshot.size) {
                                dispatch({ type: 'add_album', payload: { arr, lastEventId } });
                            }
                        });
                    } else {
                        dispatch({ type: 'unset_loading' });
                    }

                });


            } else {
                dispatch({ type: 'unset_loading' });
            }

        });
    }
}

const getSelectedEvent = (dispatch) => {
    return (eventId) => {
        dispatch({ type: 'select_event', payload: eventId });
    }
}

const addImage = (dispatch) => {
    return async (imagePath, eventId) => {
        try {
            dispatch({ type: 'set_albumloading' });
            var uniqId = new Date();
            var imageRef = firebase.storage().ref(`EventAlbums/${eventId}`).child(`${uniqId}.jpeg`);
            imageRef.putFile(imagePath).then(() => {
                firebase.storage().ref(`EventAlbums/${eventId}/${uniqId}.jpeg`).getDownloadURL().then(url => {
                    
                    firestore().collection("Events").doc(eventId).update(
                        {
                            album: firestore.FieldValue.arrayUnion(url)
                        }
                    );
                    dispatch({ type: 'update_album', payload: { url: url, id: eventId } });
                }).catch(error => {
                    dispatch({ type: 'unset_loading' });
                });


            }).catch(error => {
                dispatch({ type: 'unset_loading' });
            });
        } catch (error) {
            dispatch({ type: 'unset_loading' });
        }
    }
}

export const { Provider, Context } = createDataContext(
    albumReducer,
    { getEventAlbum, getSelectedEvent, addImage, getMoreEventAlbum, setAlbumLoading },
    { eventAlbum: [], selectEvent: {}, isLoading: false, isAlbumLoading: true, lastEventId: '', selectedAlbum: [] }
);